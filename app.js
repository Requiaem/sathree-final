/*~~~~~~~~~~~~~~~~~~~~~~~~~~
  Required Dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// WebSocket management
const ws = require('./ws');

// Path Tools
const path = require('path');

// HTTP Request Helper
const multer = require('multer');

// Logging and Debugging
const logger = require('morgan');

// HTTP framework
const express = require('express');

// Anoter HTTP Request Helper
const methodOverride = require('method-override');

// The model object
const { model } = require('./model');

/*~~~~~~~~~~~~~~~~~~~~~~~~
  App Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~*/

const app = express();                                                               		// Create the express app                                               
app.use(logger('dev'));                                                              		// Log HTTP requests
app.use(express.urlencoded({ extended: false }));                                    		// parse application/x-www-form-urlencoded
app.use(express.json({ limit: '4MB' }));                                             		// parse application/json
app.use(multer().none());                                                            		// parse multipart/form-data
app.use('/pacman', express.static(path.join(__dirname, 'pacman')));                             		// serve static files
app.use(express.static(path.join(__dirname, 'public'), { index: "index.html" }));    		// serve static files
app.use('/build', express.static(path.join(__dirname, 'node_modules/three/build'))); 		// serve three.js
app.use('/jsm', express.static(path.join(__dirname, 'node_modules/three/examples/jsm'))); 	// serve three.js optionals
app.use(methodOverride('_method'));                                                  		// override with POST having ?_method=METHOD                                
app.set('view engine', 'ejs');
app.use("/pacman/index.html", (req, res) => {
  res.render("pacman")
})

app.use('/examples', express.static(path.join(__dirname, 'node_modules/three/examples'))); 	// serve three.js optionals
// require('ejsc-views').compile();	                                                 // Setup client-side view rendering

/*~~~~~~~~~~~~~~~~~~~~~~~~~~
  API Routes Handlers
~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

const routers = require('./routes');	// Import the routers

app.use('/user', routers.user);			// Handle user routes
app.use('/record', routers.record);		// Handle record routes
app.use('/chat', routers.chat);			// Handle chat routes

/*~~~~~~~~~~~~~~~~~~~~~
  Error Handlers
~~~~~~~~~~~~~~~~~~~~~~*/

app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

/*~~~~~~~~~~~~~~~~~~~~~~~
  App Bootstrapping
~~~~~~~~~~~~~~~~~~~~~~~~*/

// Conecting...
console.log('\x1b[33m%s\x1b[0m', `Trying to boot the HTTP server...`);

// Set the port
app.set('port', process.env.PORT || 8888);
// Start the HTTP server
const server = require('http').createServer(app);
// Start the WebSocket server, attaching it to the HTTP server
ws.init(server);

// Prompts the user when the server is ready
server.on('listening', function () {
  // Connected!
  console.log('\x1b[32m%s\x1b[0m', `HTTP server listening on port ${app.get('port')}!`);
});

// Start listening for requests
server.listen(app.get('port'));


