# Mistery Cabinet

## What is the Mistery Cabinet?
The **Mistery Cabinet** is an educational web application that allows you to play the popular game [PAC-MAN](https://www.pacman.com/en/) embedded within a 3D environment. 
This is not only a fun way to play the game, but also a great way to learn 3D spatial awareness and geometry.

## Mission and Vision

The **Mistery Cabinet** is a simple, yet powerful application that introduces a complete implementation of the [three.js*](https://threejs.org/) library without the usage of additional plugins building-user interfaces libraries such as [React](https://reactjs.org/). 
Three.js abstracts many of the details of [WebGL](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API), a JavaScript API for creating and rendering dynamic [2D](https://www.splashlearn.com/math-vocabulary/geometry/2-dimensional) and [3D](https://www.splashlearn.com/math-vocabulary/geometry/3-dimensional) graphics for displaying in web browsers. 
A single JavaScript file can contain animations, cameras, effects, lighting, materials, scenery, shading, and 3D objects, among others. 
Our project has a user-friendly interface that makes it easy to work with, even for those who are not familiar with 3D programming, but want to learn how to transition their webpage applications from a 2D to a 3D environment. 
Our mission is to provide learners, engineers, software developers and graphic designers with an easy way to bring 2D and 3D content to a website.

(*) See the [advantages](https://threejs.org/manual/#en/fundamentals) of using the three.js library. 

## Collaborators
- Federico BONEZZI (@Bonni03)
- Marco FARACE (@Requiaem)
- Francesco FANTOZZI (@frafantozzi)
- Cindy GUERRERO (@cguerreroto)
- Agostino MONTI (@AgostinoMonti)
- Liu PEIYU (@Niko-creater)

## 3DView States

### Unauthed Views
#### SignedOut Orbital Nav View
  - [ ] **Login Form**
  - [ ] **Signup View TR**
  - [ ] **Info Point TR**
### Authed Views
#### SignedIn Orbital Nav View
  - [ ] **Play View TR**
  - [ ] **Profile View TR**
  - [ ] _Search Plaza View TR*_
  - [ ] **Chat Plaza View TR**
  - [ ] **Signout View Button**
  - [ ] **Info Point TR**
#### Play View 
  - [ ] **Play Button** 
  - [ ] **Credits Buttton**
  #### Profile View 
  - [ ] Image 
  - [ ] Info
  - [ ] Bio
  - [ ] Follow/Unfollow Button
  - [ ] _Follower# Nav_*
    - [ ] _Follower List Nav_*
      - [ ] _Follower Profile View_*
  - [ ] _Following# Nav_*
    - [ ] _Following List Nav_*
      - [ ] _Following Profile View_*
  - [ ] Personal HighScores List
#### Leaderboard View
 - [ ] Various Leaderboards (Global, Friends...)
#### Search Plaza View
  - [ ] Floating SearchBar
  - [ ] Results 'Plaza' Spherical Link
   - [ ] Result Profile View
#### Chat Plaza view
  - [ ] Mutual Follow ChatRooms
  - [ ] General Chat Plaza
  - [ ] Embedded Search Plaza View
  
  Elements in _this format_* are nice-to-have's and will probably be discarded before deadline.
  
  ## Backend Implementation
  
  ### Mongo DB Atlas Collections' Models
  
  #### User
  - [ ] _id           : __ObjectId__
  - [ ] email         : __String__
  - [ ] password      : __String__
  - [ ] username      : __String__
  - [ ] image_url     : __String__
  - [ ] firstname     : __String__
  - [ ] lastname      : __String__
  - [ ] bio           : __String__
  - [ ] followers      : __Id[]__
  - [ ] following     : __Id[]__
  
  #### GameRecord
  - [ ] _id           : __ObjectId__
  - [ ] _user_id      : __Id__
  - [ ] _game_id      : __Integer__
  - [ ] points        : __Integer__
  - [ ] level         : __Integer__
  - [ ] time          : __Float__
  
  #### Chat
  - [ ] _id           : __ObjectId__
  - [ ] name          : __String__
  - [ ] _party_ids    : __Id[]__
  - [ ] history       : __Message[]__
  
  #### Message
  - [ ] text          : __String__
  - [ ] timestamp     : __Date__
  - [ ] _user_id      : __Id__
  
  ### API Routes
  
  #### How authorization will work
  
  1) __client-side__ socket.emit('auth.login', formdata);
  2) __server-side__ socket.on('auth.login' (formdata) => { let user_id, token = validateLogin(formadata) })
    
  3) if request was validated:

  3a) socket.join('authed'); 
      socket.join(':id'); 
      authed.add(user_id, token); 
      io.to(':id').emit('authed', token);
      
  else
  
  3b) nothing is emitted
  
  
  Server-Side only authentication model:
  Dictionary<_user_id, token> authed
  
  #### User Routes
  - [x] _/_ - __GET__ authed user                             (AUTHED)
  - [x] _/:id_ - __GET__ user by id                           (AUTHED)
  - [x] _/search/:query_ - __GET__ users by query             (AUTHED)
  - [x] _/friends - __GET__ authenticated user's friends      (AUTHED)
  - [x] - __POST__ new user                                   ()
  - [x] _/edit_ - __PUT__ existing user                       (AUTHED)
  - [x] _/follow/:id_ - __PUT__ existing users                (AUTHED)
  - [x] _/unfollow/:id_ - __PUT__ existing users              (AUTHED)
  - [x] _/delete - __DELETE__ existing user                   (AUTHED)
  
  #### Record Routes
  - [ ] _/_ - __GET__ authed user records                                       (AUTHED) 
  - [ ] _/_ - __POST__ register a new score                                     (AUTHED)
  - [ ] _/:id_ - __GET__ user records by user_id                                (AUTHED)
  - [ ] _/:game/:level_ - __GET__ records by game and level                     (AUTHED)
  - [ ] _/friends/:game/:level_ - __GET__ friends' records by game and level    (AUTHED) 
  
  #### Chat Routes
  - [ ] _/_ - __GET__ all authed user's chat rooms                                (AUTHED)
  - [ ] _/:id_ - __GET__ a specific chat room by id                               (AUTHED, OWNED)
  - [ ] _/add/:chat_id/:user_ids_ - __PUT__ party members or create new chat      (AUTHED, OWNED)
  - [ ] _/remove/:chat_id/:user_ids_ - __PUT__ party members or create new chat   (AUTHED, OWNED)
  - [ ] _/:id/message_ - __PUT__ a new message in the history                     (AUTHED, OWNED)
  - [ ] _/new/:users_ - __POST__ a new chat room                                  (AUTHED)




