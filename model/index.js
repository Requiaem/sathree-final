const mongodb = require('mongodb');
const uuidv4 = require('uuid').v4;

/**************************
         DB MODEL
**************************/

// DB connection parameters
const db_name = 'ThreeDB';
const db_user = 'requiaem';
const db_password = '9zD5afjTinhaLEel';
const db_collections = ['Users', 'Records', 'ChatRooms'];
const db_uri = `mongodb+srv://requiaem:9zD5afjTinhaLEel@threedb.pwv1fhx.mongodb.net/?retryWrites=true&w=majority`;

// Model and Client objects
const model = {};
const client = new mongodb.MongoClient(db_uri);

// Connecting...
console.log('\x1b[33m%s\x1b[0m', `Trying to connect to "${db_name}" database...`);

// Try to connect to the database
client
    .connect()
    // If the connection is successful
    .then(client => {
        // Connected!
        console.log('\x1b[32m%s\x1b[0m', `Connected to ${db_name} database as ${db_user}!`);

        // Assign the database to the exported model
        model.db = client.db(db_name);
        
        // Assign the collections to the exported model
        db_collections.forEach(c => {
            model[c] = model.db.collection(c);
        })
    })
    // Handle errors
    .catch(err => console.error(err));

model.EmailExists = function (email) {
    return model.Users.findOne({ email: email })
}

model.UsernameExists = function (username) {
    return model.Users.findOne({ username: username })
}

/**************************
        AUTH MODEL
**************************/

const auth = {};
auth.data = {};

auth.Authorize = function (_id) {
    // generate a new uuid token
    const token = uuidv4();
    // save the token key in the auth data with the user id as the value
    auth.data[token] = _id;
    // return the token
    console.log(auth.data);
    return token;
}

auth.RevokeToken = function (token) {
    // delete the token key from the auth data
    delete auth.data[token];
}

auth.RevokeID = function (_id) {
    // find the token key with the user id as the value
    const token = Object.keys(auth.data).find(key => auth.data[key] === _id);
    // delete the token key from the auth data
    delete auth.data[token];
}

auth.IsAuthorized = function (token, _id) {
    // check if the token exists in the auth data

    console.log("Authorizing Token: " + token);
    console.log("Authorizing ID: " + _id);
    console.log(auth.data[token]);
    if (auth.data[token]) {
        // check if the token value is the same as the user id
        if (auth.data[token] === _id) {
            // return true
            return true;
        }
    }
    // return false
    return false;
}

// Export the model
exports.model = model;
exports.auth = auth;