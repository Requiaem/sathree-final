let { model, auth } = require("./model");


/**************************
        IO INSTANCE
**************************/
const io = require('socket.io')();

/**********************************
    Socket Server Initialization
***********************************/

function init(server) {
  // Attach the socket server to the HTTP server
  console.log('\x1b[33m%s\x1b[0m', `Trying to Initialize and attach the Socket server...`);
  io.attach(server);
  console.log('\x1b[32m%s\x1b[0m', `Socket server initialized and attached!`);

  // Handle socket connections
  io.on('connection', function (socket) {
    // Handle connecting clients
    if (socket.handshake.headers["is-client"] == "true") {
      handlepacmangame(socket);
    } else {
      socket.on('message', function (msg) {
        console.log('message: ', msg);

        //send to all clients
        io.emit('message', msg);

        //send to all other clients
        socket.broadcast.emit('message', msg);
      });

      console.log('client connected');
      console.log(socket.id);
      socket.emit('message', socket + ' connected');

      socket.on('user.record', (data) => {
        console.log('user.record', data);
        socket.broadcast.emit('user.record', data); //TODO: profile:id
      });

      socket.on("user.edit", (data) => {
        console.log('user.edit', data);
        socket.broadcast.emit('user.edit', data); //TODO: profile:id
      });

      socket.on("follower.count", (data) => {
        console.log('follower.count', data);
        socket.broadcast.emit('follower.count', data); //TODO: profile:id
      });

      socket.on("followed.count", (data) => {
        console.log('followed.count', data);
        socket.broadcast.emit('followed.count', data); //TODO: profile:id
      });
    }
    // TODO: Handle connecting clients
    console.log('client connected');
    console.log(socket.id);
    socket.emit('message', socket + ' connected');

    // User Authentication/Login/Logout Socket
    socket.on('auth.login', (res) => {
      const { email, password } = res;

      const emailExists = model.EmailExists(email);
      Promise.all([emailExists]).then(value => {
        if (value) {
          model.Users.findOne({ email: email }).then(
            user => {
              if (user) {
                if (user.password === password) {
                  const token = auth.Authorize(user._id.toString());
                  socket.join('authed');
                  socket.emit("authorized", { token, user });
                } else {
                  socket.emit("unauthorized", { message: "Password not correct" });
                }
              } else {
                socket.emit("unauthorized", { message: "User not found" });
              }
            }).catch(err => {
              socket.emit("unauthorized", { message: "Internal Server Error" })
            })
        }
      })
    });

    socket.on('auth.logout', (res) => {
      const { token, _id } = res;
      if (auth.IsAuthorized(token, _id)) {
        auth.RevokeToken(token);
        socket.leave('authed');
        socket.emit('unauthorized');
      }
    });

    socket.on('auth.register', (res) => {
      const { email, password, username, image_url, firstname, lastname, bio } = res;

      const emailExists = model.EmailExists(email);
      const usernameExists = model.UsernameExists(username);

      Promise.all([emailExists, usernameExists]).then(values => {
        if ((values[0] || values[1])) {
          socket.emit('unauthorized');
        } else {
          // insert a new user into the Users collection and get the _id of the new user
          model.Users.insertOne({ email: email, password: password, username: username, image_url: image_url, firstname: firstname, lastname: lastname, bio: bio, followers: [], following: [] })
            .then(res => {
              // Authenticate the user
              const token = auth.Authorize(res.insertedId.toString());
              console.log(token);
              console.log(res);
              let user = {
                _id: res.insertedId,
                email: email, 
                password: password, 
                username: username, 
                image_url: image_url,
                firstname: firstname, 
                lastname: lastname, 
                bio: bio, 
                followers: [], 
                following: []
              }
              socket.join('authed');
              // Send the token back to the client
              socket.emit('authorized', { token, user });
            }).catch(err => {
              console.log(err);
              // There was an error inserting the user into the Users collection
              socket.emit('unauthorized');
            });
        }
      })
    });

    socket.on('chat.join', (res) => {
      if (socket.rooms.has('authed')) {
        console.log(res.chatroom_id);
        socket.join(`chat-${res.chatroom_id}`)
      }
    });

    socket.on('chat.message', (res) => {
      let id = res.chatroom_id;
      console.log(id);
      io.to(`chat-${id}`).emit('chat.message', { chatroom_id: id })
    });

    socket.on("chat.new_room", () => {
      io.to(`authed`).emit('chat.new_room')
    })

    socket.on("chat.update_friends_list", () => {
      io.to('authed').emit(`chat.update_friends_list`);
    })

  });

}

function handlepacmangame(socket) {
  console.log('client connected');
  console.log(socket.id);
  socket.emit('message', socket + ' connected');
  // TODO: Complete Handle Pacman Game
}



/******************************
    EventBus Initialization
*******************************/

const EventEmitter = require('events');
const eventBus = new EventEmitter();

// Handle events
eventBus.on('custom.event', function (event) {
  // TODO: Handle Custom Event
});

/*********************
    Module Exports
**********************/

module.exports.init = init;
module.exports.eventBus = eventBus;