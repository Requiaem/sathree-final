//EJS Compiled Views - This file was automatically generated on Mon Dec 19 2022 12:30:21 GMT+0100 (Central European Standard Time)
ejs.views_include = function(locals) {
    console.log("views_include_setup",locals);
    return function(path, d) {
        console.log("ejs.views_include",path,d);
        return ejs["views_"+path.replace(/\//g,"_")]({...d,...locals}, null, ejs.views_include(locals));
    }
};
ejs.views_chat_index = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<div class=\"chat\">\n    <main class=\"room_list\">\n\n    </main>\n\n    <main class=\"room_page\">\n        <h1>\n            <%= greeting + current_user %>\n        </h1>\n        <form class=\"create_room\">\n            <p>Create a New Room:</p>\n            <input type=\"text\" id=\"msg1\">\n            <button class=\"submit\"> Send</button>\n        </form>\n    </main>\n\n    <main class=\"friend_list\">\n        <div class=\"search\">\n            <p>Search User:</p>\n            <input type=\"text\">\n        </div>\n        <div class=\"user_list\">\n\n        </div>\n    </main>\n</div>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<div class=\"chat\">\n    <main class=\"room_list\">\n\n    </main>\n\n    <main class=\"room_page\">\n        <h1>\n            ")
    ; __line = 8
    ; __append(escapeFn( greeting + current_user ))
    ; __append("\n        </h1>\n        <form class=\"create_room\">\n            <p>Create a New Room:</p>\n            <input type=\"text\" id=\"msg1\">\n            <button class=\"submit\"> Send</button>\n        </form>\n    </main>\n\n    <main class=\"friend_list\">\n        <div class=\"search\">\n            <p>Search User:</p>\n            <input type=\"text\">\n        </div>\n        <div class=\"user_list\">\n\n        </div>\n    </main>\n</div>")
    ; __line = 26
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_chat_room = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<p class=\"room_name\">\n    <%= room_name %>\n    <img class=\"add_user\" src=\"./images/add-user.svg\" height=\"20px\" width=\"20px\">\n</p>\n\n<form class=\"add\">\n    <input type=\"text\" id=\"msg1\">\n    <button class=\"submit\" id=\"add_button\"> Add </button>\n</form>\n\n<div class=\"message\">\n    <% for(let i=0; i < body.history.length; i++) { %>\n        <% let time=body.history[i].timestamp %>\n            <div class=\"msg\">\n                <div class=\"stamp\">\n                    <p class=\"name\">\n                        <%= body.history[i].user_fullname %>\n                    </p>\n                    <p class=\"time\">\n                        <%= time.substring(0, 9) + \" \" + time.substring(11, 19) %>\n                    </p>\n                </div>\n                <p class=\"text\">\n                    <%= body.history[i].text %>\n                </p>\n            </div>\n            <% } %>\n\n</div>\n\n<form class=\"send\">\n    <input type=\"text\" id=\"msg2\">\n    <button class=\"submit\">Send</button>\n</form>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<p class=\"room_name\">\n    ")
    ; __line = 2
    ; __append(escapeFn( room_name ))
    ; __append("\n    <img class=\"add_user\" src=\"./images/add-user.svg\" height=\"20px\" width=\"20px\">\n</p>\n\n<form class=\"add\">\n    <input type=\"text\" id=\"msg1\">\n    <button class=\"submit\" id=\"add_button\"> Add </button>\n</form>\n\n<div class=\"message\">\n    ")
    ; __line = 12
    ;  for(let i=0; i < body.history.length; i++) { 
    ; __append("\n        ")
    ; __line = 13
    ;  let time=body.history[i].timestamp 
    ; __append("\n            <div class=\"msg\">\n                <div class=\"stamp\">\n                    <p class=\"name\">\n                        ")
    ; __line = 17
    ; __append(escapeFn( body.history[i].user_fullname ))
    ; __append("\n                    </p>\n                    <p class=\"time\">\n                        ")
    ; __line = 20
    ; __append(escapeFn( time.substring(0, 9) + " " + time.substring(11, 19) ))
    ; __append("\n                    </p>\n                </div>\n                <p class=\"text\">\n                    ")
    ; __line = 24
    ; __append(escapeFn( body.history[i].text ))
    ; __append("\n                </p>\n            </div>\n            ")
    ; __line = 27
    ;  } 
    ; __append("\n\n</div>\n\n<form class=\"send\">\n    <input type=\"text\" id=\"msg2\">\n    <button class=\"submit\">Send</button>\n</form>")
    ; __line = 34
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_login = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<main class=\"login_page\">\n    <input type=\"checkbox\" id=\"check\" aria-hidden=\"true\">\n\n\n    <form class=\"login\">\n        <label for=\"check\" aria-hidden=\"true\">Login</label>\n        <input type=\"email\" name=\"email\" placeholder=\"Email\" required=\"\">\n        <input type=\"password\" name=\"pswd\" placeholder=\"Password\" required=\"\">\n        <input class=\"submit\" type=\"submit\" value=\"Login\">\n    </form>\n\n\n\n    <form class=\"signup\">\n        <label for=\"check\" aria-hidden=\"true\">Sign up</label>\n\t\t<input type=\"text\" name=\"firstname\" placeholder=\"First name\" required=\"\">\n\t\t<input type=\"text\" name=\"lastname\" placeholder=\"Last name\" required=\"\">\n        <input type=\"text\" name=\"username\" placeholder=\"User name\" required=\"\">\n        <input type=\"email\" name=\"email\" placeholder=\"Email\" required=\"\">\n        <input type=\"password\" name=\"pswd\" placeholder=\"Password\" required=\"\">\n        <input class=\"submit\" type=\"submit\" value=\"Sign up\">\n    </form>\n</main>\n\n\n<script src=\"js/logic/chat_client.js\"></script>\n"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<main class=\"login_page\">\n    <input type=\"checkbox\" id=\"check\" aria-hidden=\"true\">\n\n\n    <form class=\"login\">\n        <label for=\"check\" aria-hidden=\"true\">Login</label>\n        <input type=\"email\" name=\"email\" placeholder=\"Email\" required=\"\">\n        <input type=\"password\" name=\"pswd\" placeholder=\"Password\" required=\"\">\n        <input class=\"submit\" type=\"submit\" value=\"Login\">\n    </form>\n\n\n\n    <form class=\"signup\">\n        <label for=\"check\" aria-hidden=\"true\">Sign up</label>\n		<input type=\"text\" name=\"firstname\" placeholder=\"First name\" required=\"\">\n		<input type=\"text\" name=\"lastname\" placeholder=\"Last name\" required=\"\">\n        <input type=\"text\" name=\"username\" placeholder=\"User name\" required=\"\">\n        <input type=\"email\" name=\"email\" placeholder=\"Email\" required=\"\">\n        <input type=\"password\" name=\"pswd\" placeholder=\"Password\" required=\"\">\n        <input class=\"submit\" type=\"submit\" value=\"Sign up\">\n    </form>\n</main>\n\n\n<script src=\"js/logic/chat_client.js\"></script>\n")
    ; __line = 27
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_logout = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<main class=\"login_page\">\n    <input type=\"checkbox\" id=\"check\" aria-hidden=\"true\">\n\n\n    <form class=\"login logout\">\n        <input class=\"submit\" type=\"submit\" value=\"Logout\">\n    </form>\n</main>\n\n\n<script src=\"js/logic/chat_client.js\"></script>\n"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<main class=\"login_page\">\n    <input type=\"checkbox\" id=\"check\" aria-hidden=\"true\">\n\n\n    <form class=\"login logout\">\n        <input class=\"submit\" type=\"submit\" value=\"Logout\">\n    </form>\n</main>\n\n\n<script src=\"js/logic/chat_client.js\"></script>\n")
    ; __line = 12
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_pacman = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<html>\n\n<head>\n  <title>\n    Pac-Man\n  </title>\n  <style>\n    @import url('https://fonts.googleapis.com/css2?family=Alkalami&display=swap');\n  </style>\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n  <link href=\"/stylesheet/play.css\" rel=\"stylesheet\" />\n  <link href=\"/stylesheet/style.css\" rel=\"stylesheet\" />\n  <script src=\"/js/pacman/Maps.js\"></script>\n\n</head>\n\n\n\n<body>\n\n  <div class=\"main\">\n    <header>\n      <button id=\"easy\">\n        Map 1\n      </button>\n      <button id=\"mid\">\n        Map 2\n      </button>\n      <button id=\"hard\">\n        Map 3\n      </button>\n    </header>\n\n    <main class=\"main_play\">\n\n      <header>\n        <p class=\"player\"> Player:\n          <span>\n          </span>\n        </p>\n        <p class=\"score\"> Score:\n          <span>\n          </span>\n        </p>\n        <p class=\"time\"> Time:\n          <span>\n          </span>\n        </p>\n      </header>\n\n      <section class=\"game\">\n\n        <canvas id=\"gameCanvas\">\n\n        </canvas>\n\n        <section class=\"actions\">\n          <button data-action=\"start\">\n            Start\n          </button>\n          <button data-action=\"quit\">\n            Quit\n          </button>\n        </section>\n\n      </section>\n    </main>\n\n  </div>\n\n  <script src=\"/js/pacman/Game.js\" type=\"module\"></script>\n\n</body>\n\n</html>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<html>\n\n<head>\n  <title>\n    Pac-Man\n  </title>\n  <style>\n    @import url('https://fonts.googleapis.com/css2?family=Alkalami&display=swap');\n  </style>\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n  <link href=\"/stylesheet/play.css\" rel=\"stylesheet\" />\n  <link href=\"/stylesheet/style.css\" rel=\"stylesheet\" />\n  <script src=\"/js/pacman/Maps.js\"></script>\n\n</head>\n\n\n\n<body>\n\n  <div class=\"main\">\n    <header>\n      <button id=\"easy\">\n        Map 1\n      </button>\n      <button id=\"mid\">\n        Map 2\n      </button>\n      <button id=\"hard\">\n        Map 3\n      </button>\n    </header>\n\n    <main class=\"main_play\">\n\n      <header>\n        <p class=\"player\"> Player:\n          <span>\n          </span>\n        </p>\n        <p class=\"score\"> Score:\n          <span>\n          </span>\n        </p>\n        <p class=\"time\"> Time:\n          <span>\n          </span>\n        </p>\n      </header>\n\n      <section class=\"game\">\n\n        <canvas id=\"gameCanvas\">\n\n        </canvas>\n\n        <section class=\"actions\">\n          <button data-action=\"start\">\n            Start\n          </button>\n          <button data-action=\"quit\">\n            Quit\n          </button>\n        </section>\n\n      </section>\n    </main>\n\n  </div>\n\n  <script src=\"/js/pacman/Game.js\" type=\"module\"></script>\n\n</body>\n\n</html>")
    ; __line = 75
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_profile = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<div id=\"profile\">\n    <div class=\"username\">\n        <%= user.username %>\n    </div>\n    <div class=\"fullname\">\n        <%= user.firstname + \" \" + user.lastname %>\n    </div>\n    <div class=\"email\">\n        <%= user.email %>\n    </div>\n    <div class=\"bio\">\n        <%= user.bio %>\n    </div>\n    <% if(relation != \"Self\") { %>\n        <button class=\"follow\"><%= relation %></button>\n        <button class=\"chat\">Have a talk</button>\n    <% } %>\n</div>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<div id=\"profile\">\n    <div class=\"username\">\n        ")
    ; __line = 3
    ; __append(escapeFn( user.username ))
    ; __append("\n    </div>\n    <div class=\"fullname\">\n        ")
    ; __line = 6
    ; __append(escapeFn( user.firstname + " " + user.lastname ))
    ; __append("\n    </div>\n    <div class=\"email\">\n        ")
    ; __line = 9
    ; __append(escapeFn( user.email ))
    ; __append("\n    </div>\n    <div class=\"bio\">\n        ")
    ; __line = 12
    ; __append(escapeFn( user.bio ))
    ; __append("\n    </div>\n    ")
    ; __line = 14
    ;  if(relation != "Self") { 
    ; __append("\n        <button class=\"follow\">")
    ; __line = 15
    ; __append(escapeFn( relation ))
    ; __append("</button>\n        <button class=\"chat\">Have a talk</button>\n    ")
    ; __line = 17
    ;  } 
    ; __append("\n</div>")
    ; __line = 18
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_room_list = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<div class=\"list\">\n    <% for(let i=0; i < room_list.length; i++) { %>\n        <div class=\"room\" id=<%=room_list[i]._id %>><%= room_list[i].name %>\n        </div>\n        <% } %>\n</div>\n<div class=\"create\">Create in a new room!</div>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ; __append("<div class=\"list\">\n    ")
    ; __line = 2
    ;  for(let i=0; i < room_list.length; i++) { 
    ; __append("\n        <div class=\"room\" id=")
    ; __line = 3
    ; __append(escapeFn(room_list[i]._id ))
    ; __append(">")
    ; __append(escapeFn( room_list[i].name ))
    ; __append("\n        </div>\n        ")
    ; __line = 5
    ;  } 
    ; __append("\n</div>\n<div class=\"create\">Create in a new room!</div>")
    ; __line = 7
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}

ejs.views_user_list = function(locals, escapeFn, include = ejs.views_include(locals), rethrow
) {
rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
  var lines = str.split('\n');
  var start = Math.max(lineno - 3, 0);
  var end = Math.min(lines.length, lineno + 3);
  var filename = esc(flnm);
  // Error context
  var context = lines.slice(start, end).map(function (line, i){
    var curr = i + start + 1;
    return (curr == lineno ? ' >> ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'ejs') + ':'
    + lineno + '\n'
    + context + '\n\n'
    + err.message;

  throw err;
};
escapeFn = escapeFn || function (markup) {
  return markup == undefined
    ? ''
    : String(markup)
      .replace(_MATCH_HTML, encode_char);
};
var _ENCODE_HTML_RULES = {
      "&": "&amp;"
    , "<": "&lt;"
    , ">": "&gt;"
    , '"': "&#34;"
    , "'": "&#39;"
    }
  , _MATCH_HTML = /[&<>'"]/g;
function encode_char(c) {
  return _ENCODE_HTML_RULES[c] || c;
};
;
var __line = 1
  , __lines = "<% for(let i=0; i < user_list.length; i++) { %>\n    <div class=\"user\" id=<%=user_list[i]._id %>>\n        <div class=\"user_icon\">\n            <img src=\"./images/user.png\" height=\"20px\" width=\"20px\">\n            <div class=\"name\">\n                <%= user_list[i].username %>\n            </div>\n        </div>\n        <div class=\"status\">\n            <div class=\"follow_status\"> following </div>\n            <div class=\"online_status\"> online</div>\n        </div>\n    </div>\n    <% } %>"
  , __filename = undefined;
try {
  var __output = "";
  function __append(s) { if (s !== undefined && s !== null) __output += s }
  with (locals || {}) {
    ;  for(let i=0; i < user_list.length; i++) { 
    ; __append("\n    <div class=\"user\" id=")
    ; __line = 2
    ; __append(escapeFn(user_list[i]._id ))
    ; __append(">\n        <div class=\"user_icon\">\n            <img src=\"./images/user.png\" height=\"20px\" width=\"20px\">\n            <div class=\"name\">\n                ")
    ; __line = 6
    ; __append(escapeFn( user_list[i].username ))
    ; __append("\n            </div>\n        </div>\n        <div class=\"status\">\n            <div class=\"follow_status\"> following </div>\n            <div class=\"online_status\"> online</div>\n        </div>\n    </div>\n    ")
    ; __line = 14
    ;  } 
  }
  return __output;
} catch (e) {
  rethrow(e, __lines, __filename, __line, escapeFn);
}

}