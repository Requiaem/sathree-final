import { FontLoader } from 'three/FontLoader';

const loader = new FontLoader();

const loadText = (callback = (font) => {}) => loader.load(
	// resource URL
	'/assets/fonts/helvetika.json',

	// onLoad callback
	function ( font ) {
		// do something with the font
		callback(font);
	},

	// onProgress callback
	function ( xhr ) {
		console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
	},

	// onError callback
	function ( err ) {
		console.log( 'An error happened' );
	}
);

export { loadText };