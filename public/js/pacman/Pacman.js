import PacmanDirection from "./PacmanDirection.js";
import {score} from "./Game.js";
import * as THREE from "three";

export default class Pacman {

  constructor(x, y, pacmanSize, velocity, mapp) {
    // horizontal position
    this.x = x;
    // vertical position
    this.y = y;
    this.pacmanSize = pacmanSize;

    this.mapp = mapp;
    this.#pacmanLoader();

    this.actualDirection = null;
    this.requestedDirection = null;

    this.pacmanMouthTimerDefault = 10;
    this.pacmanMouthTimer = null;

    this.pacmanTurnDirection = this.turn.up;
    console.log("adding keydown listener")
    document.onkeydown = this.#keydown;

    this.pacmanStartsToMove = false;

    this.specialFoodEffectStart = false;
    this.specialFoodEffectEnd = false;

    this.timerArr = [];

    this.velocity = velocity;
    //the score, updated when a food is eaten
    this.playerScore = 0;


  }
  //array defined in order to turn pacman when the direction change
  turn = {
    right: 0,
    down: 1,
    left: 2,
    up: 3,
  }

  draw(ctx, stopGame, ghosts) {
    //if the game is not over:
    if (!stopGame) {
      //keep moving
      this.#pacmanMouth();
      this.#pacmanMovement();
    }

    this.#eatFood();
    this.#eatSpecialFood();
    this.#eatGhosts(ghosts);

    const size = this.pacmanSize / 2;

    ctx.save();
    ctx.translate(this.x + size, this.y + size);
    //rotation
    ctx.rotate((this.pacmanTurnDirection * 90 * Math.PI) / 180);
    // draw Image of the pacman 
    ctx.drawImage(
      this.pacmanImages[this.pacmanImagesIndex],
      -size,
      -size,
      this.pacmanSize,
      this.pacmanSize
    );
    // console.log("hi:" + ctx.restore);
    ctx.restore();
  }

  #pacmanLoader() {
    // Load all pacman's images
    let pac1 = new Image();
    pac1.src = "/pictures/pacmanFull.png";

    let pac2 = new Image();
    pac2.src = "/pictures/pacmanSemi.png";

    let pac3 = new Image();
    pac3.src = "/pictures/pacmanWide.png";

    let pac4 = new Image();
    pac4.src = "/pictures/pacmanFull.png";

    this.pacmanImages = [
      pac1,
      pac2,
      pac3,
      pac4,
    ];

    this.pacmanImagesIndex = 0;
  }

  // movments keys
  #keydown = (e) => {
    // up
    if (e.keyCode == 38 || e.keyCode == 87) {
      if (this.actualDirection == PacmanDirection.down) {
        this.actualDirection = PacmanDirection.up;
      }
      this.requestedDirection = PacmanDirection.up;
      this.pacmanStartsToMove = true;
    }
    // down
    if (e.keyCode == 40 || e.keyCode == 83) {
      if (this.actualDirection == PacmanDirection.up) {
        this.actualDirection = PacmanDirection.down;
      }
      this.requestedDirection = PacmanDirection.down;
      this.pacmanStartsToMove = true;
    }
    // left
    if (e.keyCode == 37 || e.keyCode == 65) {
      if (this.actualDirection == PacmanDirection.right) {
        this.actualDirection = PacmanDirection.left;
      }
      this.requestedDirection = PacmanDirection.left;
      this.pacmanStartsToMove = true;

    }
    // right
    if (e.keyCode == 39 || e.keyCode == 68) {
      if (this.actualDirection == PacmanDirection.left) {
        this.actualDirection = PacmanDirection.left;
      }
      this.requestedDirection = PacmanDirection.right;
      this.pacmanStartsToMove = true;

    }
  }
  
  #pacmanMovement() {
    if (this.actualDirection !== this.requestedDirection) {
      // if the direction in which the pacman is going is different from the key pressed in the moment
      if (Number.isInteger(this.x / this.pacmanSize) && Number.isInteger(this.y / this.pacmanSize)) {
        if (!this.mapp.detectCollision(
          this.x, this.y, this.requestedDirection
        ))
        // set the direction of the pacman equal to the one requested
          this.actualDirection = this.requestedDirection;
      }
    }
    if (this.mapp.detectCollision(this.x, this.y, this.actualDirection)) {
      this.pacmanMouthTimer = null;
      this.pacmanImagesIndex = 1;
      return;
    } else if (this.actualDirection != null && this.pacmanMouthTimer == null) {
      this.pacmanMouthTimer = this.pacmanMouthTimerDefault;
    }

    if (this.actualDirection == PacmanDirection.up) {
      this.y -= this.velocity;
      this.pacmanTurnDirection = this.turn.up;
      return;
    } else if (this.actualDirection == PacmanDirection.down) {
      this.y += this.velocity;
      this.pacmanTurnDirection = this.turn.down;
      return;
    } else if (this.actualDirection == PacmanDirection.left) {
      this.x -= this.velocity;
      this.pacmanTurnDirection = this.turn.left;
      return;
    } else if (this.actualDirection == PacmanDirection.right) {
      this.x += this.velocity;
      this.pacmanTurnDirection = this.turn.right;
      return;
    }
  }

  // method to make the mouth move
  #pacmanMouth() {
    if (this.pacmanMouthTimer == null) {
      return;
    }
    this.pacmanMouthTimer--;
    if (this.pacmanMouthTimer == 0) {
      this.pacmanMouthTimer = this.pacmanMouthTimerDefault;
      this.pacmanImagesIndex++;
    }
    if (this.pacmanImagesIndex == this.pacmanImages.length) {
      this.pacmanImagesIndex = 0;
    }
  }

  #eatFood() {
    if (this.mapp.eatFood(this.x, this.y)) {
      // whenever a food is eaten update the score
      score.innerHTML = this.playerScore++;
    }
  }

  #eatSpecialFood() {
    // the ghost will get scared
    if (this.mapp.eatSpecialFood(this.x, this.y)) {
      // whenever a Specialfood is eaten update the score
      score.innerHTML = this.playerScore++;
      // also start the effect of the food
      this.specialFoodEffectStart = true;
      this.specialFoodEffectEnd = false;
      this.timerArr.forEach((t) => clearTimeout(t));
      this.timerArr = [];

      let powerDotTimer = setTimeout(() => {
        // set the duration of the effect
        this.specialFoodEffectStart = false;
        this.specialFoodEffectEnd = false;
      }, 1000 * 6);


      this.timerArr.push(powerDotTimer);

      let specialFoodAlmostExpired = setTimeout(() => {
        // set the duration of the effect vanishing
        this.specialFoodEffectEnd = true;
      }, 1000 * 3);
      
      this.timerArr.push(specialFoodAlmostExpired);
    }
  }
  #eatGhosts(ghosts) {
    // if pacman ate a specialFood;
    if (this.specialFoodEffectStart == true) {
      // check if during this time pacman collides with some ghosts;
      const ghostKilled = ghosts.filter((ghost) => ghost.collisionWith(this)); // this = pacman.
      // then remove with splice from the ghosts array all the ghost that were eaten;
      ghostKilled.forEach((ghost) => {
        ghosts.splice(ghosts.indexOf(ghost), 1);

      })
    }
  }
}
