import PacmanDirection from "./PacmanDirection.js";
export default class Redpac {

  constructor(x, y, redpacSize, velocity, mapp) {
    this.x = x;
    this.y = y;
    this.redpacSize = redpacSize;
    this.velocity = velocity;
    this.mapp = mapp;

    this.#redpacLoader();

    this.redpacDirection = Math.floor(Math.random() * Object.keys(PacmanDirection).length);

    this.timerDefault = this.rand(1, 3);
    this.timer = this.timerDefault;
    // this.timer = this.rand(0, 40);


  }

  draw(ctx, stopGame, redpac) {
    // if the game is not stopped
    if (!stopGame) {
      //keep this enemy moving
      this.#movingRedpac();
      this.#redpacDirection();
    }
    this.#setRedpacImage(ctx, redpac);
  }

  collisionWith(pacman) {
    let size = this.redpacSize / 2;
    // if there is a collision return true
    if (this.x < pacman.x + size && this.x + size > pacman.x && this.y + size > pacman.y && this.y < pacman.y + size) {
      return true;
    } else {
      return false;
    }
  }

  #setRedpacImage(ctx) {
    // set the image
    this.img = this.redpac;
    // and draw it
    ctx.drawImage(
      this.img,
      this.x,
      this.y,
      this.redpacSize,
      this.redpacSize
    );
  }

  rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  #redpacDirection() {
    // decrement the timer
    this.timer--;
    let newDirection = null;
    // when the timer reaches 0
    if (this.timer == 0) {
      this.timer = this.timerDefault;
      // set a new direction 
      newDirection = Math.floor(
        Math.random() * Object.keys(PacmanDirection).length
      );
    }

    if (newDirection != null && this.#redpacDirection != newDirection) {
      // check if the enemy is inside a tile, meaning everythi=ing is working correctly
      if (
        Number.isInteger(this.x / this.redpacSize) &&
        Number.isInteger(this.y / this.redpacSize)
      ) {
        // if it is:
        if (
          // if there isn't a collision
          !this.mapp.detectCollision(
            this.x,
            this.y,
            newDirection
          )
        ) {
          // set the direction equal to the new direction
          this.redpacDirection = newDirection;
        }
      }
    }
  }

  #movingRedpac() {
    if (
      // if there is no collision
      !this.mapp.detectCollision(
        this.x,
        this.y,
        this.redpacDirection
      )
    ) {
      // move
      if (this.redpacDirection == PacmanDirection.up) {
        this.y -= this.velocity;
        return;
      } else if (this.redpacDirection == PacmanDirection.down) {
        this.y += this.velocity;
        return;
      } else if (this.redpacDirection == PacmanDirection.left) {
        this.x -= this.velocity;
        return;
      } else if (this.redpacDirection == PacmanDirection.right) {
        this.x += this.velocity;
        return;
      }
    }
  }

  #redpacLoader() {
    // Load the image of the enemy
    this.redpac = new Image();
    this.redpac.src = '/pictures/enemy.png'

    this.img = this.redpac;
  }


}

