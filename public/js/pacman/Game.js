import MapScript from "./MapScript.js";
import * as THREE from "three";
import { leaderboard_api } from "../logic/leaderboard_api.js";
import { authModel } from "../logic/auth_model.js";


//--------------------------------------------------------------------------------------
// Game
//--------------------------------------------------------------------------------------

// Size of the tiles
const tSize = 30;
// Velocity of the moving objects
const velocity = 1.5;
//set gameOver false 
let gameOver = false;
let level = 1;
//set gameWasWon false
let gameWasWon = false;
let scoreSent = false;


let mapGame;
//get the pacman
var pacman;
//get the ghosts
var ghosts;
//get the special enemy
var redpac;
//set gameInterval to null
var gameInterval = null;

// game canvas
var canvas;
//set 2d canvas
var ctx;
var mesh;
var timer;
var score;

function initGame() {
  if (gameInterval != undefined) {
    clearInterval(gameInterval);
  }
  pacman = mapGame.getPacman(velocity);
  ghosts = mapGame.getGhosts(velocity);
  redpac = mapGame.getRedpac(velocity);

  canvas = document.getElementById("gameCanvas");
  ctx = canvas?.getContext("2d");

  mapGame.setCanvasSize(canvas)
  gameInterval = setInterval(game, 1000 / 75);
  gameOver = false;
  scoreSent = false;
}

export function endGame() {
  console.log("Game Over")

  //clear the interval
  clearInterval(gameInterval);

  //set nesh and timer to null
  mesh = null;
  timer = null;
  score = null;

  // set gameOver to true since the game ended.
  gameOver = true;
  gameWasWon = false;
  gameInterval = null;
  mapGame = null; //new MapScript(tSize, mapEasy.slice())
  document.onkeydown = null;

  //get pacman, ghosts, special Enemy.
  pacman = null;
  ghosts = null;
  redpac = null;

  //set the canvas to null
  canvas = null;
  ctx = null;
}

export function goBack() {
  console.log("GoBackButton");
  clearInterval(intervalId);
  console.log(intervalId);
  time = 0;
  console.log(time);

  let buttons = document.querySelectorAll(".mapButton");
  // remove all buttons
  buttons.forEach((button) => {
    button.remove();
  })

  let timee = document.querySelectorAll('span')

  timee.forEach((time) => {
    time.remove()
  })

}

export function mapButtonCallback(map) {
  console.log("Map Button Callback");

  //set the map in term of which button was clicked;
  let mapCopy = mapEasy;
  level = 0;
  if (map == 1) {
    mapCopy = mapMid;
    level = 1;
  } else if (map == 2) {
    mapCopy = mapHard;
    level = 2;
  }
  mapCopy = mapCopy.map((line) => line.map((el) => el));
  mapGame = new MapScript(tSize, mapCopy);
  console.log(mapGame.mapGame)

  //select buttons
  let buttons = document.querySelectorAll(".mapButton");
  // remove all buttons
  buttons.forEach((button) => {
    button.remove();
  })

  //set interval
  initGame();
}

export function bindHTMLElements(_mesh, _timer, _score) {
  console.log("BINDING Canvas & Timer");
  canvas = document.getElementById("gameCanvas");
  ctx = canvas.getContext("2d");
  timer = _timer;
  mesh = _mesh;
  score = _score
}

document.getElementById("easy")?.addEventListener('click', () => {
  let mapCopy = mapEasy.map((line) => line.map((el) => el));
  mapGame = new MapScript(tSize, mapCopy);
  initGame();
})

document.getElementById("mid")?.addEventListener('click', () => {
  let mapCopy = mapMid.map((line) => line.map((el) => el));
  mapGame = new MapScript(tSize, mapCopy);
  initGame();
})

document.getElementById("hard")?.addEventListener('click', () => {
  let mapCopy = mapHard.map((line) => line.map((el) => el));
  mapGame = new MapScript(tSize, mapCopy);
  initGame();
})


// update Pac-Man 
function game() {
  mapGame.draw(ctx);
  pacman.draw(ctx, stopGame(), ghosts, redpac);
  ghosts.forEach(ghost =>
    ghost.draw(ctx, stopGame(), pacman, redpac));
  redpac.draw(ctx, stopGame(), pacman, ghosts)

  startTimer();
  //check for gameOver or Victory;
  checkingGameOver();
  checkingVictory();

  if (gameOver && !scoreSent) {
    scoreSent = true;
    leaderboard_api.post_record({
      username: authModel.currentUser.username,
      _game_id: 0,
      level: level,
      points: parseInt(score.innerHTML),
      time: time
    })
  }

  if (mesh != undefined) {
    let texture = new THREE.CanvasTexture(canvas)
    mesh.material.map = texture;
    mesh.material.emissiveMap = texture;
    mesh.material.emissive = new THREE.Color(0xffffff);
    mesh.material.emissiveIntensity = 0.95;
    mesh.material.needsUpdate = true;
  }
}

function checkingVictory() {
  // if the game has not still been won;
  if (gameWasWon != true) {
    // then check with the isWon() method the foodleft;
    gameWasWon = mapGame.isWon();
    // if the game has been won stop the game
    if (gameWasWon == true) {
      gameOver = true;
      clearInterval(intervalId);
      return;
    }
  }
}

function checkingGameOver() {
  // if the game is not over
  if (!gameOver) {
    //set gameOver equal to the func GameOver() in order to change when the game is actually over by collision.
    gameOver = GameOver();
    if (gameOver) {
      console.log("game is over");
      clearInterval(intervalId);
      return;
      //clear the time

    }
  }
}



function GameOver() {
  // return if the game ends by collision with enemies
  return ghosts.some((ghost) => !pacman.specialFoodEffectStart && ghost.collisionWith(pacman)) || redpac.collisionWith(pacman);
}

function stopGame() {
  // stop the game when it is over or before starting it.
  return !pacman.pacmanStartsToMove || gameOver || gameWasWon;
}

//--------------------------------------------------------------------------------------
// Timer
//--------------------------------------------------------------------------------------


const duration = 0;
var time = duration;


function updateTimer() {
  // create min and sec to display
  var min = Math.trunc(time / 12000);
  let sec = Math.round((time / 200) % 60);
  // console.log(time)

  //create milliseconds to display

  // put everything together inside a const display
  const display = `${min}:${sec}`;
  // display it
  timer.innerHTML = display;
}

function incrementTimer() {
  try {
    // increment the timer when the player start the game
    if ((pacman.pacmanStartsToMove)) {
      time = time + 1;
      // call updateTimer func
      updateTimer();
    }
  } catch (err) {
    return;
  }

}

function startTimer() {
  // increment the timer
  if (!gameOver) {
    // increment the timer
    incrementTimer();
    //set gameOver equal to the func GameOver() in order to change when the game is actually over by collision.
    gameOver = GameOver();
  }
  if (gameOver) {
    console.log("game is over");
    clearInterval(intervalId);
  }
}



//handle the time in the interval
const intervalId = setInterval(incrementTimer, 1);

function restartTimer() {
  clearInterval(intervalId);
  incrementTimer();

}

function clearTimer() {
  // clear the time in the interval
  clearInterval(intervalId);
  return;
}

export { score };
