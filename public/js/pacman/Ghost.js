import PacmanDirection from "./PacmanDirection.js";

// All Ghosts images;
let images = [
  "/pictures/ghostOrange.png",
  "/pictures/ghostGreen.png",
  "/pictures/ghostPink.png",
  "/pictures/ghostLightBlue.png"
];

export default class Ghost {
  //ghost constructor
  constructor(x, y, ghostSize, velocity, mapp) {
    this.x = x;
    this.y = y;
    this.ghostSize = ghostSize;
    this.velocity = velocity;
    this.mapp = mapp;

    //loads the ghosts
    this.#ghostLoader();

    //switching direction of the ghosts
    this.ghostDirection = Math.floor(Math.random() * Object.keys(PacmanDirection).length);
    this.timerDefault = this.rand(1, 5);
    this.timer = this.timerDefault;

    this.effectAlmostFinishedDefault = 10;
    this.effectAlmostFinished = this.effectAlmostFinishedDefault;

  }

  draw(ctx, stopGame, pacman) {
    //if the game is not over:
    if (!stopGame) {
      //keep the ghosts moving
      this.#movingGhost();
      this.#ghostDirection();
    }
    // switch between the images of the ghosts
    this.#setGhostImage(ctx, pacman);
  }

  collisionWith(pacman) {
    let size = this.ghostSize / 2;
    //basically return true if there is a collsion.
    if (this.x < pacman.x + size && this.x + size > pacman.x && this.y + size > pacman.y && this.y < pacman.y + size) {
      return true;
    } else {
      return false;
    }
  }

  #setGhostImage(ctx, pacman) {
    if (pacman.specialFoodEffectStart == false) {
      // if the state is false, meaning it was not eaten then use the normal ghost image;
      this.img = this.ghost1;
    } else {
      // if the special food was eaten then got to:
      this.#setGhostImageSpecialFood(pacman);
    }
    ctx.drawImage(
      this.img,
      this.x,
      this.y,
      this.ghostSize,
      this.ghostSize
    );
  }

  #setGhostImageSpecialFood(pacman) {
    //when the effect of the special food ends:
    if (pacman.specialFoodEffectEnd) {
      // decrement effectAlmostFinished
      this.effectAlmostFinished--;
      // when it reaches 0
      if (this.effectAlmostFinished === 0) {
        // go back to original
        this.effectAlmostFinished = this.effectAlmostFinishedDefault;
        if (this.img !== this.ghost2) {
          this.img = this.ghost2;
        } else {
          this.img = this.ghost3;
        }
      }
    } else {
      this.img = this.ghost2;
    }
  }

  rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  #ghostDirection() {
    this.timer--;
    let newDirection = null;
    // change direction based on the timer
    if (this.timer == 0) {
      this.timer = this.timerDefault;
      newDirection = Math.floor(
        Math.random() * Object.keys(PacmanDirection).length
      );
    }

    if (newDirection != null && this.#ghostDirection != newDirection) {
      if (
        Number.isInteger(this.x / this.ghostSize) &&
        Number.isInteger(this.y / this.ghostSize)
      ) {
        //detect the collisions 
        if (
          !this.mapp.detectCollision(
            this.x,
            this.y,
            newDirection
          )
        ) {
          this.ghostDirection = newDirection;
        }
      }
    }
  }

  #movingGhost() {
    //detect the collisions
    if (
      !this.mapp.detectCollision(
        this.x,
        this.y,
        this.ghostDirection
      )
    ) {
      // make them move
      if (this.ghostDirection == PacmanDirection.up) {
        this.y -= this.velocity;
        return;
      } else if (this.ghostDirection == PacmanDirection.down) {
        this.y += this.velocity;
        return;
      } else if (this.ghostDirection == PacmanDirection.left) {
        this.x -= this.velocity;
        return;
      } else if (this.ghostDirection == PacmanDirection.right) {
        this.x += this.velocity;
        return;
      }
    }
  }

  #ghostLoader() {
    // this.ghost1 = new Image();
    // this.ghost1 = this.images[Math.floor(Math.random() * this.images.length)];

    //loads the pictures of the ghosts
    this.ghost1 = new Image();
    this.ghost1.src = images[Math.floor(Math.random() * images.length)];

    this.ghost2 = new Image();
    this.ghost2.src = '/pictures/ghostBlue.png';

    this.ghost3 = new Image();
    this.ghost3.src = '/pictures/ghostWhite.png';

    this.img = this.ghost1;
  }


}
// let ghost = new Ghost
// console.log(ghost);
// console.log(ghost.rand(1,3));
