import Pacman from "./Pacman.js";
import PacmanDirection from "./PacmanDirection.js";
import Ghost from "./Ghost.js";
import Redpac from "./Redpac.js";


//--------------------------------------------------------------------------------------
// Map
//--------------------------------------------------------------------------------------
export default class MapScript {
  constructor(tSize, mapGame) {
    //The Size of every square in the game
    this.tSize = tSize;

    // walls
    this.wall = new Image();
    this.wall.src = "/pictures/wall.png"

    // food
    this.food = new Image();
    this.food.src = "/pictures/food.png"

    // special food
    this.specialFood = new Image();
    this.specialFood.src = "/pictures/specialFood.png";

    // needed for the ticking later;
    this.sFood = this.sFood;

    // timers for the specialFood;
    this.specialFoodStandardTimer = 50;
    this.specialFoodTimer = this.specialFoodStandardTimer;

    this.mapGame = mapGame.map(x => x);
  }

  draw(ctx) {
    // i  = row , j = column;
    for (let i = 0; i < this.mapGame.length; i++) {
      for (let j = 0; j < this.mapGame[i].length; j++) {
        let t = this.mapGame[i][j];
        if (t === 1) { // 1 === walls;
          this.#drawWall(ctx, j, i, this.tSize);
        } else if (t === 0) { // 0 === food;
          this.#drawFood(ctx, j, i, this.tSize);
        } else if (t === 5) { // 5 === specialFood;
          this.#drawSpecialFood(ctx, j, i, this.tSize);
        } else {
          this.#drawBlank(ctx, j, i, this.tSize);
        }

      }
    }
  }

  setCanvasSize(canvas) {
    canvas.width = this.mapGame[0].length * this.tSize;
    canvas.height = this.mapGame.length * this.tSize;
  }

  #drawWall(ctx, y, x, size) {
    ctx.drawImage(
      this.wall,
      y * this.tSize,
      x * this.tSize,
      size,
      size
    );
  }

  #drawSpecialFood(ctx, y, x, size) {
    //substracting to the timer
    this.specialFoodTimer--;
    if (this.specialFoodTimer === 0) {
      //setting it back
      this.specialFoodTimer = this.specialFoodStandardTimer;
      //if the current image is the pink image
      if (this.sFood == this.specialFood) {
        //then switch to the normal food to give a sensation of beeping
        this.sFood = this.food;
      } else {
        // otherwise switch to the pinkFood;
        this.sFood = this.specialFood;
      }
    }
    // Draw this on the canvas;
    try {
      ctx.drawImage(
        this.sFood,
        y * size,
        x * size,
        size,
        size
      );
    } catch (error) {
      return;
    }
  }

  #drawFood(ctx, y, x, size) {
    ctx.drawImage(
      this.food,
      y * this.tSize,
      x * this.tSize,
      size,
      size
    );
  }

  #drawBlank(ctx, y, x, size) {
    ctx.fillStyle = "black";
    ctx.fillRect(
      y * this.tSize,
      x * this.tSize,
      size,
      size
    );
  }

  getPacman(velocity) {
    for (let x = 0; x < this.mapGame.length; x++) {
      for (let y = 0; y < this.mapGame[x].length; y++) {
        let t = this.mapGame[x][y];
        if (t === 2) {
          this.mapGame[x][y] = 0;
          return new Pacman(y * this.tSize, x * this.tSize, this.tSize, velocity, this);
        }
      }
    }
  }

  getGhosts(velocity) {
    let ghosts = []; //array of ghosts

    for (let x = 0; x < this.mapGame.length; x++) {
      for (let y = 0; y < this.mapGame[x].length; y++) {
        const t = this.mapGame[x][y];
        if (t === 4) {
          this.mapGame[x][y] = 0;
          ghosts.push(new Ghost(
            y * this.tSize,
            x * this.tSize,
            this.tSize,
            velocity,
            this));
        }
      }
    }
    return ghosts;
  }

  getRedpac(velocity) {
    for (let x = 0; x < this.mapGame.length; x++) {
      for (let y = 0; y < this.mapGame[x].length; y++) {
        let t = this.mapGame[x][y];
        if (t === 6) {
          this.mapGame[x][y] = 0;
          return new Redpac(y * this.tSize, x * this.tSize, this.tSize, velocity, this);
        }
      }
    }
  }

  detectCollision(x, y, dir) {
    if (dir == null) {
      return;
    }
    if (
      Number.isInteger(x / this.tSize) &&
      Number.isInteger(y / this.tSize)
    ) {
      let j = 0; // col
      let i = 0; // row
      let nextJ = 0; // next col 
      let nextI = 0; // next row

      switch (dir) {
        case PacmanDirection.right:
          nextJ = x + this.tSize;
          j = nextJ / this.tSize;
          i = y / this.tSize;
          break;

        case PacmanDirection.left:
          nextJ = x - this.tSize;
          j = nextJ / this.tSize;
          i = y / this.tSize;
          break;

        case PacmanDirection.up:
          nextI = y - this.tSize;
          i = nextI / this.tSize;
          j = x / this.tSize
          break;

        case PacmanDirection.down:
          nextI = y + this.tSize;
          i = nextI / this.tSize;
          j = x / this.tSize
          break;
      }
      // t === tile;
      const t = this.mapGame[i][j];
      if (t === 1) {
        return true;
      }
    }
    return false;
  }

  eatSpecialFood(x, y) {
    let row = y / this.tSize;
    let col = x / this.tSize;
    // to check if he is in a grid.
    if (Number.isInteger(row) && Number.isInteger(col)) {
      const t = this.mapGame[row][col];
      // if it was eaten 
      if (t === 5) {
        // then set that spot to empty
        this.mapGame[row][col] = null;
        // return true since it was eaten
        return true;
      }

    } //else return false
    return false;
  }

  eatFood(x, y) {
    let row = y / this.tSize;
    let col = x / this.tSize;
    //check if it is inside a grid
    if (Number.isInteger(row) && Number.isInteger(col)) {
      //if pacman goes over a tile where there is food,
      if (this.mapGame[row][col] === 0) {
        // set it to null so that then it is empty, food was eaten.
        this.mapGame[row][col] = null;
        return true;
      }
    }
    // else if the tile has not food, don't do anything
    return false;
  }

  isWon() {
    // if there are no more tiles equal to zero the game was won.
    return this.#foodleft() === 0;
  }

  #foodleft() {
    // flat makes the (2d array) matrix into a normal (1d) array.
    return this.mapGame.flat().filter(t => t === 0).length;
    // check if every tile is equal to 0 to so if all food was eaten.
  }

}

