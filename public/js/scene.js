import { OrbitControls } from 'three/OrbitControls'
import { TWEEN } from 'three/tween'
import * as THREE from 'three'
import {
  sceneDefinition,
  onMouseClick,
} from './logic/scene_logic.js'

import { live_scene } from './scenes/live_scene.js'
import { light_scene } from './scenes/light_scene.js'
import { unauthed_nav } from './scenes/nav_scene.js'


/********************/
/*   Scene Setup    */
/********************/

// set all of the document's text font to be the same = Monospace
document.body.style.fontFamily = 'Helvetica'

sceneDefinition.cssControls = new OrbitControls(sceneDefinition.camera, sceneDefinition.cssRenderer.domElement)
sceneDefinition.cssRenderer.domElement.style.position = 'absolute';
sceneDefinition.cssRenderer.domElement.style.top = 0;
sceneDefinition.cssRenderer.domElement.style.zIndex = 99;
sceneDefinition.cssControls.enableDamping = true
sceneDefinition.cssControls.dampingFactor = 0.05
sceneDefinition.cssControls.enablePan = false
sceneDefinition.cssControls.minDistance = 3
sceneDefinition.cssControls.maxDistance = 15

sceneDefinition.camera.position.set(-2, 2.5, 5.8)
sceneDefinition.cssControls.update()

sceneDefinition.cameraInitialPos = sceneDefinition.camera.position.clone()
sceneDefinition.cameraInitialRot = sceneDefinition.camera.quaternion.clone()

sceneDefinition.threeRenderer.setSize(window.innerWidth, window.innerHeight)
sceneDefinition.cssRenderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(sceneDefinition.cssRenderer.domElement)
document.body.appendChild(sceneDefinition.threeRenderer.domElement)
window.addEventListener('click', onMouseClick, false)

/***********************/
/*   Scene Definition  */
/***********************/

// The nav scene
unauthed_nav();

// The live scene
live_scene();                                 // this is what will be seen in production

// The light scene
const directional_light = light_scene();      // separate setup for lights

// The debug scene
// debug_scene();                                // this is what will be seen in development

/********************/
/*   Scene Update   */
/********************/

window.addEventListener(
  'resize',
  () => {
    sceneDefinition.camera.aspect = window.innerWidth / window.innerHeight
    sceneDefinition.camera.updateProjectionMatrix()
    sceneDefinition.threeRenderer.setSize(window.innerWidth, window.innerHeight)
    sceneDefinition.cssRenderer.setSize(window.innerWidth, window.innerHeight)
    render()
  },
  false
)

// update the scene
function animate() {
  requestAnimationFrame(animate)
  TWEEN.update()
  render()
}

// render the scene
function render() {
  // make the lights follow the camera
  sceneDefinition.camera.aspect = window.innerWidth / window.innerHeight
  sceneDefinition.camera.updateProjectionMatrix()
  sceneDefinition.threeRenderer.setSize(window.innerWidth, window.innerHeight)
  sceneDefinition.cssRenderer.setSize(window.innerWidth, window.innerHeight)
  directional_light.position.copy(sceneDefinition.camera.position)
  directional_light.quaternion.copy(sceneDefinition.camera.quaternion)
  sceneDefinition.camera.position.multiplyScalar(500);
  sceneDefinition.cssRenderer.render(sceneDefinition.cssScene, sceneDefinition.camera)
  sceneDefinition.camera.position.divideScalar(500);
  sceneDefinition.threeRenderer.render(sceneDefinition.threeScene, sceneDefinition.camera)
}

animate()