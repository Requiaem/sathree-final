export const authModel = {
    currentUser : null,
    token : {
        id : null,
        key : null,
    },
    authed : false,
    logout: () => {
        authModel.authed = false;
        authModel.currentUser = null;
        authModel.token.id = null;
        authModel.token.key = null;
    },
    header: () => {
        return `Bearer ${authModel.token.key}/${authModel.token.id}` 
    }
}