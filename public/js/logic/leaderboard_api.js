import { authModel } from "./auth_model.js";
import { api } from "./api_model.js";

export const leaderboard_api = function () {

  function get_leaderboard(game) {
    return api._fetchJSON("GET", `/record/${game}`, { Authorization: authModel.header() });
  }

  function get_level_leaderboard(game, level) {
    return api._fetchJSON("GET", `/record/${game}/${level}`, { Authorization: authModel.header() });
  }

  function get_user_leaderboard(user_id, game, level) {
    return api._fetchJSON("GET", `/record/user/${user_id}/${game}`, { Authorization: authModel.header() });
  }

  function post_record(record){
    return api._fetchJSON("POST", `/record`, { Authorization: authModel.header() }, record)
  }

  function get_friends_leaderboard(game, level) {
    return api._fetchJSON("GET", `/record/friends/${game}`, { Authorization: authModel.header() });
  }

  return {
    _fetchJSON: api._fetchJSON,
    get_leaderboard,
    get_level_leaderboard,
    get_user_leaderboard,
    post_record,
    get_friends_leaderboard
  }
}();



