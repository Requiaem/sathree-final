import { authModel } from "./auth_model.js";
import { socket } from "./socket_model.js";
import { api } from "./api_model.js";
import { onExitScene } from "./scene_logic.js";
import { unauthed_nav } from "../scenes/nav_scene.js";

export var main_container;

export const chat_api = function () {

  function get_chatroom_list() {
    return api._fetchJSON("GET", "/chat", { Authorization: authModel.header() });
  }

  function get_user_by_username(username, r_id, container) {
    api._fetchJSON("GET", "/user/name/" + username, { Authorization: authModel.header() }).then(body => {
      if (body.error) {
        alert(body.error);
      } else {
        join_room(r_id, body._id, container);
      }
    })
  }

  async function create_new_room(name, container, o_id) {

    let state = true;

    let chatroom_list = await get_chatroom_list();

    chatroom_list.forEach(a => {
      if (a.name === name) {
        state = false;
      }
    })

    if (state) {

      api._fetchJSON("POST", "/chat/new/" + o_id, { Authorization: `Bearer ${authModel.token.key}` }, { _id: authModel.token.id, name: name }).then(body => {

        socket.emit("chat.new_room")
        
        get_chatroom(body.insertedId, container);
      })
    } else {
      alert("Room Existed!")
    }
  }

  function join_room(r_id, n_id, container) {
    api._fetchJSON("PUT", "/chat/add/" + r_id + "/" + n_id, { Authorization: `Bearer ${authModel.token.key}` }, { _id: authModel.token.id }).then(body => {
      console.log("************")
      if (body.modifiedCount === 0) {
        alert("User has already in room")

        socket.emit("chat.new_room")
      } else {
        alert("Add success!")
      }
      get_chatroom(r_id, container);
    })
  }

  function follow_fun(user_name, o_id, rel, container) {
    if (rel === "Follow") {
      api._fetchJSON("PUT", "/user/follow/" + o_id, { Authorization: `Bearer ${authModel.token.key}` }, { _id: authModel.token.id }).then(body => {
        console.log("follow success!");
        get_profile(user_name, container);
        friends_fun(container);
      })
    } else if (rel === "Unfollow") {
      api._fetchJSON("PUT", "/user/unfollow/" + o_id, { Authorization: `Bearer ${authModel.token.key}` }, { _id: authModel.token.id }).then(body => {
        console.log("unfollow success!");
        get_profile(user_name, container);
        friends_fun(container);
      })
    } else {
      console.log("Follow operation fails");
    }
  }

  function chatroom_list_fun(container) {
    get_chatroom_list().then(room_list => {
      container.querySelector("main.room_list").innerHTML = ejs.views_room_list({ room_list });

      container.querySelectorAll("div.room").forEach(a => {
        let sub_id = a.id;
        a.addEventListener("click", (event) => {
          event.preventDefault();

          a.style.border = "thick solid #0000FF";
          get_chatroom(sub_id, container);
          
        })
      })

      welcome_fun(container);
    })
  }


  // Go back to welcome page function
  function welcome_fun(container) {
    container.querySelector("div.create").addEventListener("click", (event) => {
      event.preventDefault();
      render_chat(container);
    })
  }


  // Get friends list function
  function friends_fun(container) {
    api._fetchJSON("GET", "/user/friends", { Authorization: authModel.header() }).then(user_list => {
      container.querySelector("div.user_list").innerHTML = ejs.views_user_list({ user_list })
      get_profile_fun(container);
    })
  }

  // Open profile function
  function get_profile(user_name, container) {
    api._fetchJSON("GET", "/user/search/" + user_name).then(user_list => {

      let user = user_list[0];

      let user_fullname = user.username;

      let relation;

      if (user.followers.includes(authModel.token.id)) {
        relation = "Unfollow"
      } else[
        relation = "Follow"
      ]

      container.querySelector("main.room_page").innerHTML = ejs.views_profile({ user, relation });

      container.querySelector("button.follow").addEventListener("click", (event) => {
        event.preventDefault();
        
        socket.emit("chat.update_friends_list")

        follow_fun(user.username, user._id, relation, container);
      })

      // Have a chat with your friend and you haven't talk before then create a new room
      container.querySelector("button.chat").addEventListener("click", async (event) => {
        event.preventDefault();
        let room_list = await get_chatroom_list();

        let exist = false;
        let exist_room_name;
        let exist_room_id;

        room_list.forEach(a => {
          if (a.name === (authModel.currentUser.username + "-" + user_fullname) || a.name === (user_fullname + "-" + authModel.currentUser.username)) {
            exist_room_name = a.name; // check if there is personal room
            exist_room_id = a._id;
            exist = true;
          }
        })

        // Create a new room if it doesn't exsit or go to personal chatroom
        if (exist) {
          get_chatroom(exist_room_id, container);
        } else {
          create_new_room(authModel.currentUser.username + "-" + user_fullname, container, user._id);
        }
      })

      welcome_fun(container);
    })
  }

  function send_message(r_id, mes, container) {
    api._fetchJSON("PUT", "/chat/" + r_id + "/message", { Authorization: `Bearer ${authModel.token.key}` }, { _id: authModel.token.id, text: mes }).then(body => {
      console.log(body);

      socket.emit("chat.message", { chatroom_id: r_id });

      get_chatroom(r_id, container);

      document.querySelector(".message").scrollTop = document.querySelector(".message").scrollHeight;
    })
  }

  // search user function
  function search_user_fun(container) {
    let input = container.querySelector("main.friend_list div.search input");
    input.addEventListener("input", () => {
      if (input.value !== "") {
        api._fetchJSON("GET", "/user/search/" + input.value).then(user_list => {
          container.querySelector("div.user_list").innerHTML = ejs.views_user_list({ user_list })
          get_profile_fun(container);
        })
      }
    })
  }

  // Get get profile function
  function get_profile_fun(container) {
    container.querySelectorAll("div.user").forEach(a => {
      a.addEventListener("click", (event) => {
        event.preventDefault();
        let user = a.querySelector("div.name").innerHTML;
        get_profile(user, container);
      })
    })
  }

  // Get chatroom function
  function get_chatroom(r_id, container) {
    api._fetchJSON("GET", "/chat/" + r_id, { Authorization: authModel.header() }).then(async body => {
      console.log(body);
      let room_name;
      let room_list = await get_chatroom_list();
      let hoster = false;

      socket.emit("chat.join", { chatroom_id: r_id });

      room_list.forEach(a => { // Check if current user own the room
        if (a._id === r_id) {
          room_name = a.name;
          if (a._party_ids[0] === authModel.token.id) {
            hoster = true;
          }
        }
      })
      container.querySelector(".room_page").innerHTML = ejs.views_chat_room({ body, room_name });
      document.querySelector(".message").scrollTop = document.querySelector(".message").scrollHeight;
      // Display the add user to room from when the current user is room holder
      if (hoster) {
        let display = false;
        let add_user_icon = container.querySelector("img.add_user");
        add_user_icon.style.display = "inline";
        add_user_icon.addEventListener("click", (event) => {
          display = !display;
          event.preventDefault();
          let add_user_form = container.querySelector("form.add");

          if (display) {
            add_user_form.style.display = "inline";
          } else {
            add_user_form.style.display = "none";
          }

          container.querySelector("#add_button").addEventListener("click", (event) => {
            event.preventDefault();

            let input_content = container.querySelector("form.add input#msg1").value;

            console.log(input_content);
            get_user_by_username(input_content, r_id, container);
          })
        })
      }

      // Send out the message

      container.querySelector("form.send button.submit").addEventListener("click", (event) => {
        event.preventDefault();
        let message = container.querySelector("main.room_page input#msg2").value;

        send_message(r_id, message, container);
      })

      chatroom_list_fun(container);

      welcome_fun(container);

      friends_fun(container);

      search_user_fun(container);

    })
  }

  // render the welcome page
  async function render_chat(container) {

    main_container = container;

    let time = new Date();

    console.log(time.getHours());

    var greeting;

    // Greeting according to the time
    if (time.getHours() >= 6 && time.getHours() <= 12) {
      greeting = "Good Morning!  "
    } else if (time.getHours() > 12 && time.getHours() < 19) {
      greeting = "Good Afternoon!  "
    } else {
      greeting = "Good Evening!  "
    }

    container.innerHTML = ejs.views_chat_index({ greeting, current_user: authModel.currentUser.username });

    // Create a new room function
    let message_page = main_container.querySelector("main.room_page div.message");
    container.querySelector("main.room_page button").addEventListener("click", (event) => {
      event.preventDefault();
      let name = container.querySelector("form.create_room input").value;

      if (name) {
        create_new_room(name, container, authModel.token.id);
      }
    })


    friends_fun(container);

    chatroom_list_fun(container);

    search_user_fun(container);
  }

  function login(em, psw) {

    const data = { email: em, password: psw }

    socket.emit("auth.login", data)

  }

  function logout() {
    socket.emit("auth.logout", { token: authModel.token.key, _id: authModel.token.id })
  }

  function register(first, last, usr, em, psw) {

    const data = { email: em, password: psw, username: usr, firstname: first, lastname: last }

    socket.emit("auth.register", data)
  }

  return {
    _fetchJSON: api._fetchJSON,
    login,
    register,
    logout,
    get_chatroom,
    get_chatroom_list,
    chatroom_list_fun,
    render_chat,
    friends_fun
  }
}();



