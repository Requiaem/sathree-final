import * as THREE from 'three'
import { TWEEN } from 'three/tween'
import { routs } from '../scenes/nav_scene.js'
import { button_scene } from '../scenes/button_scene.js'
import { CSS3DRenderer, CSS3DObject } from 'three/CSS3DRenderer'
import { mapButtonCallback, goBack, endGame } from '../pacman/Game.js'

const mouse = new THREE.Vector2()
let selectedObject = null

const sceneDefinition = {
  threeRenderer: new THREE.WebGLRenderer({ antialias: false, powerPreference: 'high-performance' }),
  cssRenderer: new CSS3DRenderer(),
  threeScene: new THREE.Scene(),
  cssScene: new THREE.Scene(),
  camera: new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 100),
  cssControls: null,
  cameraInitialPos: null,
  cameraInitialRot: null,
  game_canvas: null,
  pacman_element: null,
  pacman_mesh: null,
  signs: [],
  state: {
    auth: false,
    scene: false,
  }
}

const findTarget = (position, rotation) => {
  // compute direction vector of new rotation, starting from new position
  const direction = new THREE.Vector3(0, 0, -1)
  direction.applyQuaternion(rotation)
  direction.add(position)
  return direction
}

// animate camera based on position and rotation
const animateCamera = (position, rotation, startControl = false, isGame = false) => {
  !startControl ? button_scene("Go Back", () => { onExitScene(), goBack() }, 0, 'exitButton') : () => { }
  isGame ? button_scene("Map I", () => { mapButtonCallback(0) }, 1, 'mapButton') : () => { }
  isGame ? button_scene("Map II", () => { mapButtonCallback(1) }, 2, 'mapButton') : () => { }
  isGame ? button_scene("Map III", () => { mapButtonCallback(2) }, 3, 'mapButton') : () => { }
  sceneDefinition.cssControls.enabled = false
  let finished = false
  // transition slowly to the new position
  new TWEEN.Tween(sceneDefinition.camera.position)
    .to(position)
    .easing(TWEEN.Easing.Quadratic.Out)
    .onComplete(() => {
      if (finished) {
        sceneDefinition.cssControls.target = !startControl ? findTarget(sceneDefinition.camera.position, rotation) : new THREE.Vector3(0, 0, 0)
        sceneDefinition.cssControls.enabled = startControl ? true : false
        // cssControls.enabled = true
      } else {
        finished = true
      }
    })
    .start()
  // transition slowly to the new rotation
  new TWEEN.Tween(sceneDefinition.camera.quaternion)
    .to(rotation)
    .easing(TWEEN.Easing.Quadratic.Out)
    .onComplete(() => {
      if (finished) {
        sceneDefinition.cssControls.target = !startControl ? findTarget(sceneDefinition.camera.position, rotation) : new THREE.Vector3(0, 0, 0)
        sceneDefinition.cssControls.enabled = startControl ? true : false
        // cssControls.enabled = true
      } else {
        finished = true
      }
    })
    .start()
}

// nav click
const onMouseClick = (event) => {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
  // detect intersections 
  const raycaster = new THREE.Raycaster()
  raycaster.setFromCamera(mouse, sceneDefinition.camera)
  const intersects = raycaster.intersectObjects(sceneDefinition.threeScene.children, true)
  selectedObject = null
  if (intersects.length > 0) {
    console.log(intersects[0].object)
    // if the clicked object is a sign, or a child of a sign, select the sign
    if (!sceneDefinition.state.scene && !selectNav(intersects[0].object)) {
      intersects[0].object?.traverseAncestors((obj) => {
        selectNav(obj)
      })
    }
  }
  console.log(sceneDefinition.state.scene)
}

const onExitScene = () => {
  sceneDefinition.state.scene = false

  // remove all objects from the css scene
  sceneDefinition.cssScene.children.forEach((obj) => {
    sceneDefinition.cssScene.remove(obj)
  })

  if (sceneDefinition.pacman_mesh != null) {
    endGame()
    sceneDefinition.threeScene.remove(sceneDefinition.pacman_mesh)
  }

  if (sceneDefinition.pacman_element != null) {
    sceneDefinition.pacman_element.parentNode.removeChild(sceneDefinition.pacman_element)
    sceneDefinition.pacman_element = null
  }

  document.getElementsByClassName('exitButton')[0]?.remove()


  // remove the "Exit" button
  sceneDefinition.threeScene.remove(sceneDefinition.threeScene.getObjectByName('Exit'))

  // reset the camera to default position and rotation
  animateCamera(sceneDefinition.cameraInitialPos, sceneDefinition.cameraInitialRot, true)
}

const selectNav = (obj) => {
  if (obj != null && routs.hasOwnProperty(obj.name)) {
    try {
      routs[obj.name].callback()
      animateCamera(routs[obj.name].position, routs[obj.name].rotation, false, routs[obj.name].isGame)
      selectedObject = obj
      sceneDefinition.state.scene = true
      return true
    } catch (e) {
      console.log(e)
      return false
    }
  } else {
    return false
  }
}

export {
  sceneDefinition,
  onMouseClick,
  onExitScene,
  selectNav,
  animateCamera,
  findTarget
}