import { authModel } from "./auth_model.js";
import { authed_nav, unauthed_nav } from "../scenes/nav_scene.js";
import { onExitScene } from "../logic/scene_logic.js";
import { chat_api, main_container } from "./chat_api.js";

export const socket = io();

socket.on("unauthorized", (msg) => {
    // alert(msg);
    if(authModel.authed) {
        authModel.logout();
        unauthed_nav();
        onExitScene();
    }
})

socket.on("chat.message", (res) => {
    if (main_container.querySelector("main.room_page")) {
        chat_api.get_chatroom(res.chatroom_id, main_container);
        document.querySelector(".message").scrollTop = document.querySelector(".message").scrollHeight;
    }
})

socket.on("authorized", (data) => {
    if(!authModel.authed) {
        authed_nav();
        onExitScene();
    }

    authModel.authed = true;
    authModel.token.id = data.user._id;
    authModel.token.key = data.token;
    authModel.currentUser = data.user;
    console.log(authModel.token);
})

socket.on("chat.new_room", () => {
    if (main_container.querySelector("main.room_list")) {
        chat_api.chatroom_list_fun(main_container);   
    }
})

socket.on("chat.update_friends_list", () => {
    if (main_container.querySelector("main.friend_list")) {
        chat_api.friends_fun(main_container);
    }
})