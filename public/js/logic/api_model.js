export const api = function () {

    function validateResponse(res) {

        if (res.status == 200 || res.status == 201) {
            return res.json();
        } else if (res.status === 409 || res.status == 404) {
            return res.json();
        } else if (res.status == 204) {
            return;
        } else {
            throw res.status;
        }
    }


    async function _fetchJSON(method, url, headers, body) {


        function addHeaders(headers) {

            let newHeaders = { ...headers };

            newHeaders['Accept'] = 'application/json';

            if (method == 'POST' || method == 'PUT' || method == 'PATCH')
                newHeaders['Content-Type'] = 'application/json';
            return newHeaders;
        }


        if (method === 'POST' || method == 'PUT' || method == 'PATCH') {
            body = JSON.stringify(body);
        }

        const res = await fetch(url, { method, headers: addHeaders(headers), body });
        return validateResponse(res);

    }
    return {
        _fetchJSON,
    }
}();