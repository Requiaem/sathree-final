import { CSS3DObject } from 'three/CSS3DRenderer'
import { onExitScene, sceneDefinition } from '../logic/scene_logic.js'
import { chat_api } from '../logic/chat_api.js'
import { authed_nav } from '../scenes/nav_scene.js'
import { socket } from '../logic/socket_model.js'
import { authModel } from '../logic/auth_model.js'


/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const login_scene = (position, rotation) => {
    // create a container for the form
    const container = document.createElement('div')
    // make the container a fixed size
    container.style.width = '180px'
    container.style.height = '35px'


    // use ejs load the login page
    container.innerHTML = ejs.views_login()

    // 
    container.querySelector("form.login").addEventListener("submit", event => {
        event.preventDefault();
        console.log("login form submitted");
        let email = container.querySelector("form.login input[name='email']").value;
        let password = container.querySelector("form.login input[name='pswd']").value;
        chat_api.login(email, password);
    })

    container.querySelector("form.signup").addEventListener("submit", event => {
        event.preventDefault();
        let email = container.querySelector("form.signup input[name='email']").value;
        let password = container.querySelector("form.signup input[name='pswd']").value;
        let user_name = container.querySelector("form.signup input[name='username']").value;
        let first_name = container.querySelector("form.signup input[name='firstname']").value;
        let last_name = container.querySelector("form.signup input[name='lastname']").value;
        chat_api.register(first_name, last_name, user_name, email, password);
    })



    // make the form a CSS3DObject
    const formObject = new CSS3DObject(container)
    formObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    //formObject.rotation.set(rotation.x, rotation.y, rotation.z)
    formObject.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w)
    formObject.scale.set(0.18, 0.18, 0.18)
    sceneDefinition.cssScene.add(formObject)

    // // add transform controls
    // const transformControls = new TransformControls(sceneDefinition.camera, sceneDefinition.renderer.domElement)
    // transformControls.attach(formObject)
    // sceneDefinition.cssScene.add(transformControls)

    // // add event listeners for position change
    // transformControls.addEventListener('dragging-changed', (event) => {
    //     // console log the position of the form
    //     console.log(formObject.position)
    //     // console log the rotation of the form
    //     console.log(formObject.quaternion)
    // })
}
export { login_scene }
