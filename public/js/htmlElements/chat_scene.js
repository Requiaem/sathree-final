import { CSS3DObject } from 'three/CSS3DRenderer'
import { sceneDefinition } from '../logic/scene_logic.js'
import { chat_api } from '../logic/chat_api.js'
import { socket } from '../logic/socket_model.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const chat_scene = (position, rotation) => {
    // create a container for the form
    const container = document.createElement('div')
    // make the container a fixed size
    container.style.width = '1286px'
    container.style.height = '938px'

    // add an html form to the CSS3DRenderer scene

    chat_api.render_chat(container)

    socket.on('chat.message', (res) => {
        chat_api.get_chatroom(res.chatroom_id, container)
    })

    // make the form a CSS3DObject
    const formObject = new CSS3DObject(container)
    formObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    formObject.rotation.set(rotation.x, rotation.y, rotation.z)
    formObject.scale.set(1.2, 1.2, 1.2)
    sceneDefinition.cssScene.add(formObject)
}
export { chat_scene }
