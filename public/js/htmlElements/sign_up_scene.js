import { CSS3DObject } from 'three/CSS3DRenderer'
import { sceneDefinition } from '../logic/scene_logic.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const sign_up_scene = (position, rotation) => {
    // create a container for the form
    const container = document.createElement('div')
    // make the container a fixed size
    container.style.width = '180px'
    container.style.height = '35px'

    // add an html form to the CSS3DRenderer scene
    const form = document.createElement('form')
    form.innerHTML = `
    <label for="firstname">First Name</label>
    <label for="lastname">Last Name</label>
    <input type="text" id="firstname" name="firstname" placeholder="Firstname" required>
    <input type="text" id="lastname" name="lastname" placeholder="Lastname">
    <label for="email">Email</label> 
    <input type="email" id="email" name="email" placeholder="Email" required>
    <label for="password">Password</label>
    <input type="password" id="password" name="password" placeholder="Password" required>
    <label for="username">Username</label>
    <input type="text" id="username" name="username" placeholder="Username" required>
    <input type="submit" value="SignUp">
    `

    // set color text to white
    form.style.color = 'white'

    // set a grid layout
    form.style.display = 'grid'
    form.style.gridTemplateColumns = 'repeat(4, 1fr)'
    form.style.gridTemplateRows = 'repeat(9, 1fr)'
    form.style.Gap = '0px 10px'

    form.querySelector('label[for="firstname"]').style.gridArea = '1 / 1 / 2 / 3' 
    form.querySelector('label[for="lastname"]').style.gridArea = '1 / 3 / 2 / 5'
    form.querySelector('input[id="firstname"]').style.gridArea = '2 / 1 / 3 / 3'
    form.querySelector('input[id="lastname"]').style.gridArea = '2 / 3 / 3 / 5'
    form.querySelector('label[for="email"]').style.gridArea = '3 / 1 / 4 / 5'
    form.querySelector('input[id="email"]').style.gridArea = '4 / 1 / 5 / 5'
    form.querySelector('label[for="password"]').style.gridArea = '5 / 1 / 6 / 5'
    form.querySelector('input[id="password"]').style.gridArea = '6 / 1 / 7 / 5'
    form.querySelector('label[for="username"]').style.gridArea = '7 / 1 / 8 / 5'
    form.querySelector('input[id="username"]').style.gridArea = '8 / 1 / 9 / 5'
    form.querySelector('input[type="submit"]').style.gridArea = '9 / 1 / 10 / 5'

    // make the input elements a black transparent background, with a golden border
    form.querySelectorAll('input').forEach((input) => {
        input.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
        input.style.border = '1px solid goldenrod'
        input.style.borderRadius = '10px'
        input.style.margin = '7px'
        input.style.color = 'goldenrod'
        // padding is 5px
        input.style.padding = '5px'

        // onHover make the input grow in size smoothly
        input.addEventListener('mouseover', () => {
            input.style.transition = 'all 0.35s ease'
            input.style.scale = '1.2'
        })

        // onHoverOut make the input shrink in size smoothly
        input.addEventListener('mouseout', () => {
            input.style.transition = 'all 0.35s ease'
            input.style.scale = '1'
            input.style.margin = '7px'
        })
    })

    // select the submit button
    const submit = form.querySelector('input[type="submit"]')
    // make the submit button goldenrod
    submit.style.backgroundColor = 'goldenrod'
    submit.style.color = 'black'
    submit.style.fontWeight = 'bold'

    // make the label text golden
    form.querySelectorAll('label').forEach((label) => {
        label.style.color = 'goldenrod'
        label.style.margin = '7px'
        label.style.alignSelf = 'flex-end'
    })

    form.style.width = '100%'

    // add the form to the container
    container.appendChild(form)

    // make the form a CSS3DObject
    const formObject = new CSS3DObject(container)
    formObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    formObject.rotation.set(rotation.x, rotation.y, rotation.z)
    sceneDefinition.cssScene.add(formObject)
}
export { sign_up_scene }
