import * as THREE from 'three'
import { authModel } from '../logic/auth_model.js'
import { sceneDefinition } from '../logic/scene_logic.js'
import { bindHTMLElements, mapButtonCallback } from '../pacman/Game.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const pacman_scene = (position, rotation) => {
  // add a new html button
  // const mapI = document.createElement('button')
  // mapI.id = 'easy'
  // mapI.innerHTML = 'Map I'
  // mapI.style.position = 'absolute'
  // mapI.style.top = '60px'
  // mapI.style.left = '10px'
  // mapI.style.width = '100px'
  // mapI.style.height = '50px'
  // mapI.style.fontSize = '20px'
  // mapI.style.fontWeight = 'bold'
  // mapI.style.color = 'white'
  // mapI.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  // mapI.style.border = '1px solid goldenrod'
  // mapI.style.borderRadius = '10px'
  // mapI.style.margin = '5px'

  // const mapII = document.createElement('button')
  // mapII.id = 'mid'
  // mapII.innerHTML = 'Map II'
  // mapII.style.position = 'absolute'
  // mapII.style.top = '120px'
  // mapII.style.left = '10px'
  // mapII.style.width = '100px'
  // mapII.style.height = '50px'
  // mapII.style.fontSize = '20px'
  // mapII.style.fontWeight = 'bold'
  // mapII.style.color = 'white'
  // mapII.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  // mapII.style.border = '1px solid goldenrod'
  // mapII.style.borderRadius = '10px'
  // mapII.style.margin = '5px'

  // const mapIII = document.createElement('button')
  // mapIII.id = 'hard'
  // mapIII.innerHTML = 'Map III'
  // mapIII.style.position = 'absolute'
  // mapIII.style.top = '170px'
  // mapIII.style.left = '10px'
  // mapIII.style.width = '100px'
  // mapIII.style.height = '50px'
  // mapIII.style.fontSize = '20px'
  // mapIII.style.fontWeight = 'bold'
  // mapIII.style.color = 'white'
  // mapIII.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  // mapIII.style.border = '1px solid goldenrod'
  // mapIII.style.borderRadius = '10px'
  // mapIII.style.margin = '5px'

  // // create a new html element for the pacman game (pacman.ejs)
  // sceneDefinition.game_canvas = document.createElement('canvas')
  // document.body.appendChild(sceneDefinition.game_canvas)
  // const ctx = sceneDefinition.game_canvas.getContext('2d')
  // ctx.fillStyle = 'white'
  // ctx.fillRect(0, 0, 69, 66)
  // // paint the canvas ctx white
  // sceneDefinition.game_canvas.width = 69
  // sceneDefinition.game_canvas.height = 66
  // sceneDefinition.game_canvas.style.position = 'absolute'
  // sceneDefinition.game_canvas.style.right = 0
  // sceneDefinition.game_canvas.style.top = 0
  // sceneDefinition.game_canvas.style.zIndex = 200

  // const timer = document.createElement('span')
  // timer.className = 'time'
  // timer.style.position = 'absolute'
  // timer.style.top = '10px'
  // timer.style.right = '10px'
  // timer.innerHTML = 'Time: 0'

  // // const score = document.createElement('span')
  // // score.className = 'score'
  // // score.style.position = 'absolute'
  // // score.style.top = '60px'
  // // score.style.right = '10px'

  // // document.body.appendChild(mapI)
  // // document.body.appendChild(mapII)
  // // document.body.appendChild(mapIII)
  // document.body.appendChild(timer)
  // // document.body.appendChild(score)


  // // create a 690 x 660 plane
  // const geometry = new THREE.BoxGeometry(0.69, 0.66, 0.01)
  // // create a canvas texture
  // const texture = new THREE.CanvasTexture(sceneDefinition.game_canvas)
  // // create a material with the texture
  // const material = new THREE.MeshBasicMaterial({ map: texture })
  // // create a mesh with the geometry and material
  // const mesh = new THREE.Mesh(geometry, material)
  // // set the position and rotation
  // mesh.position.set(position.x, position.y, position.z)
  // mesh.rotation.set(rotation.x, rotation.y, rotation.z)
  // // add the mesh to the scene
  // sceneDefinition.threeScene.add(mesh)

  // bindHTMLElements(timer, mesh)
  // add a new Node with the ejs.views_pacman content
  sceneDefinition.pacman_element = document.createElement('div')
  sceneDefinition.pacman_element.innerHTML = ejs.views_pacman()
  document.body.appendChild(sceneDefinition.pacman_element)

  const canvas = document.getElementById('gameCanvas')
  canvas.style.width = '690px'
  canvas.style.height = '660px'

  sceneDefinition.game_canvas = canvas


  document.getElementById('easy').addEventListener('click', () => {
    mapButtonCallback(0)
  })

  document.getElementById('mid').addEventListener('click', () => {
    mapButtonCallback(1)
  })

  document.getElementById('hard').addEventListener('click', () => {
    mapButtonCallback(2)
  })

  sceneDefinition.pacman_element.style.opacity = 0
  sceneDefinition.pacman_element.style.position = 'absolute'

  // position: absolute;
  //   top: 220px;
  //   left: 500px;
  //   /* width: 100px; */
  //   /* height: 50px; */
  //   font-size: 20px;
  //   font-weight: bold;
  //   color: white;
  //   background-color: rgba(0, 0, 0, 0.5);
  //   border: 1px solid goldenrod;
  //   border-radius: 10px;
  //   margin: 5px;
  //   z-index: 101;
  //   vertical-align: middle;
  //   display: grid;
  //   align-items: center;
  //   padding: 10px;

  const timerSpan = document.createElement('span'); 
  document.body.appendChild(timerSpan)
  const playerSpan = document.createElement('span'); 
  document.body.appendChild(playerSpan)
  const scoreSpan = document.createElement('span'); 
  document.body.appendChild(scoreSpan)

  setTimeout(() => {
  timerSpan.className = 'time'
  timerSpan.style.position = 'absolute'
  timerSpan.style.top = '50px'
  timerSpan.style.fontSize = '20px'
  timerSpan.style.fontWeight = 'bold'
  timerSpan.style.color = 'white'
  timerSpan.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  timerSpan.style.border = '1px solid goldenrod'
  timerSpan.style.borderRadius = '10px'
  timerSpan.style.display = 'grid'
  timerSpan.style.alignItems = 'center'
  timerSpan.style.padding = '10px'
  timerSpan.style.margin = '5px'
  timerSpan.style.zIndex = '101'
  timerSpan.innerHTML = '00:00'

  playerSpan.className = 'player'
  playerSpan.style.position = 'absolute'
  playerSpan.style.top = '50px'
  playerSpan.style.left = '500px'
  playerSpan.style.fontSize = '20px'
  playerSpan.style.fontWeight = 'bold'
  playerSpan.style.color = 'white'
  playerSpan.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  playerSpan.style.border = '1px solid goldenrod'
  playerSpan.style.borderRadius = '10px'
  playerSpan.style.display = 'grid'
  playerSpan.style.alignItems = 'center'
  playerSpan.style.padding = '10px'
  playerSpan.style.margin = '5px'
  playerSpan.style.zIndex = '101'
  playerSpan.innerHTML = authModel.currentUser.username

  scoreSpan.className = 'score'
  scoreSpan.style.position = 'absolute'
  scoreSpan.style.top = '50px'
  scoreSpan.style.right = '500px'
  scoreSpan.style.fontSize = '20px'
  scoreSpan.style.fontWeight = 'bold'
  scoreSpan.style.color = 'white'
  scoreSpan.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  scoreSpan.style.border = '1px solid goldenrod'
  scoreSpan.style.borderRadius = '10px'
  scoreSpan.style.display = 'grid'
  scoreSpan.style.alignItems = 'center'
  scoreSpan.style.padding = '10px'
  scoreSpan.style.margin = '5px'
  scoreSpan.style.zIndex = '101'
  scoreSpan.innerHTML = 'score'
  }, 1000)


  // add a new plane to the scene which is 1/10th the size of the canvas
  const geometry = new THREE.PlaneGeometry(0.69, 0.66)
  // create an image texture
  const texture = new THREE.TextureLoader().load('/pictures/pacmanStart.jpg');
  // create a material with the texture
  const material = new THREE.MeshStandardMaterial({ map: texture })
  // create a mesh with the geometry and material
  material.emissiveMap = texture;
  material.emissive = new THREE.Color(0xffffff);
  material.emissiveIntensity = 0.95;
  material.needsUpdate = true;
  sceneDefinition.pacman_mesh = new THREE.Mesh(geometry, material)
  bindHTMLElements(sceneDefinition.pacman_mesh, timerSpan, scoreSpan);
  // set the position and rotation
  sceneDefinition.pacman_mesh.position.set(position.x, position.y, position.z)
  sceneDefinition.pacman_mesh.rotation.set(rotation.x, rotation.y, rotation.z)
  sceneDefinition.pacman_mesh.scale.set(0.85, 0.85, 0.85)
  // add the mesh to the scene
  sceneDefinition.threeScene.add(sceneDefinition.pacman_mesh)
}
export { pacman_scene }
