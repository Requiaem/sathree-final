import { CSS3DObject } from 'three/CSS3DRenderer'
import { sceneDefinition } from '../logic/scene_logic.js'
import { leaderboard_api } from '../logic/leaderboard_api.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

var leaderboard_entries = [{
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
}, {
  name: 'John',
  score: 100,
  time: '1:00',
  level: 1
},
]
let current_entries = []

const leaderboard_entry = (entry) => {
  const _entry = document.createElement('li')
  _entry.className = 'leaderboard-entry'
  _entry.innerHTML = `
        <p class='record-username'>${entry.username}</p>
        <p>${entry.points}</p>
        <p>${entry.time}</p>
    `

  return _entry
}

const pick_level = (level) => {
  current_entries = leaderboard_entries.filter((entry) => entry.level == level)
  // sort by points first, then by time (descending)
  current_entries.sort((a, b) => {
    if (a.points > b.points) {
      return -1
    } else if (a.points < b.points) {
      return 1
    } else {
      if (a.time < b.time) {
        return -1
      } else if (a.time > b.time) {
        return 1
      } else {
        return 0
      }
    }
  })
  // show the current entries
  show_entries(current_entries)
}

const show_entries = (entries) => {
  // remove all li elements from the ol
  const leaderboard = document.querySelector('ol')
  if (leaderboard !== null && leaderboard !== undefined) {
    leaderboard.innerHTML = ''
    // add an html li to the CSS3DRenderer scene
    entries.forEach((entry) => {
      leaderboard.appendChild(leaderboard_entry(entry))
    })
  }
}

const setup_scene = (position, rotation) => {
  // create a container for the form
  const outer_container = document.createElement('div')
  const container = document.createElement('div')
  // make the container a fixed size
  container.style.display = 'grid'
  container.style.gridTemplateColumns = '1fr 1fr 1fr'
  container.style.gridTemplateRows = '1fr 0.25fr 5fr'
  container.style.gap = '5px 5px'

  // add an html header to the CSS3DRenderer scene
  const header = document.createElement('h1')
  header.innerHTML = 'Leaderboard'
  header.style.color = 'goldenrod'
  // change the header's to MonoSpace
  header.style.fontFamily = 'monospace'
  header.style.margin = '5px'
  // header is aligned at flex end and justified at center
  header.style.display = 'flex'
  header.style.flexDirection = 'row'
  header.style.justifyContent = 'center'
  header.style.alignItems = 'flex-end'
  header.style.alignContent = 'flex-end'
  container.appendChild(header)

  // add three html buttons to the CSS3DRenderer scene
  const buttons = [document.createElement('button'), document.createElement('button'), document.createElement('button')]
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].innerHTML = `Level ${i + 1}`
    buttons[i].addEventListener('click', () => pick_level(i))
    // style the buttons
    buttons[i].style.border = '1px solid goldenrod'
    buttons[i].style.borderRadius = '10px'
    buttons[i].style.padding = '5px'
    buttons[i].style.fontSize = '13px'
    buttons[i].style.color = 'white'
    buttons[i].style.backgroundColor = 'rgba(0, 0, 0, 0.5)'

    // make the pointer a cursor on hover
    buttons[i].style.cursor = 'pointer'
    // make transitions smooth
    buttons[i].style.transition = 'all 0.35s ease'


    // scale the button on hover
    buttons[i].addEventListener('mouseenter', () => {
      buttons[i].style.transform = 'scale(1.1)'
      buttons[i].style.backgroundColor = 'goldenrod'
      buttons[i].style.color = 'black'
      buttons[i].style.boxShadow = '0 0 10px 0 #000'
    })

    // scale the button back on mouse leave
    buttons[i].addEventListener('mouseleave', () => {
      buttons[i].style.transform = 'scale(1)'
      buttons[i].style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
      buttons[i].style.color = 'white'
      buttons[i].style.boxShadow = 'none'
    })

    container.appendChild(buttons[i])
  }

  // add an html ol to the CSS3DRenderer scene
  const leaderboard = document.createElement('ol')

  header.style.gridArea = '1 / 1 / 2 / 4'
  buttons[0].style.gridArea = '2 / 1 / 3 / 2'
  buttons[1].style.gridArea = '2 / 2 / 3 / 3'
  buttons[2].style.gridArea = '2 / 3 / 3 / 4'
  leaderboard.style.gridArea = '3 / 1 / 4 / 4'

  // leaderboard is a vertical list, flexbox
  leaderboard.style.display = 'flex'
  leaderboard.style.flexDirection = 'column'
  leaderboard.style.alignContent = 'stretch'
  leaderboard.style.justifyContent = 'stretch'
  leaderboard.style.overflowY = 'scroll'
  leaderboard.style.overflowX = 'hidden'
  leaderboard.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  leaderboard.style.borderRadius = '10px'
  leaderboard.style.margin = '5px'
  leaderboard.style.padding = '5px'
  leaderboard.style.width = '175px'
  leaderboard.style.paddingRight = '25px'
  leaderboard.style.height = '475px'
  leaderboard.style.scrollBehavior = 'smooth'

  // add the leaderboard to the container
  container.appendChild(leaderboard)
  outer_container.appendChild(container)

  // make the form a CSS3DObject
  const leaderBoardObject = new CSS3DObject(outer_container)
  leaderBoardObject.position.set(position.x * 500, position.y * 500, position.z * 500)
  leaderBoardObject.rotation.set(rotation.x, rotation.y, rotation.z)
  sceneDefinition.cssScene.add(leaderBoardObject)
  pick_level(0)
}


const leaderboard_scene = (position, rotation, mode = "Global", user_id = null) => {
  switch (mode) {
    case "Global":
      leaderboard_api.get_leaderboard(0).then((entries) => {
        leaderboard_entries = entries
        setup_scene(position, rotation)
      })
      break
    case "User":
      leaderboard_api.get_user_leaderboard(user_id).then((entries) => {
        leaderboard_entries = entries
        setup_scene(position, rotation)
      })
    case "Friends":
      leaderboard_api.get_friends_leaderboard(user_id).then((entries) => {
        leaderboard_entries = entries
        setup_scene(position, rotation)
      })
    default:
      break
  }
}


export { leaderboard_scene }
