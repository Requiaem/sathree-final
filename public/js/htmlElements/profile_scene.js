import { CSS3DObject } from 'three/CSS3DRenderer'
import { sceneDefinition } from '../logic/scene_logic.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const profile_scene = (position, rotation, user) => {
    const relation = "Self"
    // create a container for the form
    const container = document.createElement('div')
    container.id = "profile-container"
    container.innerHTML = ejs.views_profile({ user, relation })

    // make the form a CSS3DObject
    const profileObject = new CSS3DObject(container)
    profileObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    profileObject.rotation.set(rotation.x, rotation.y, rotation.z)
    sceneDefinition.cssScene.add(profileObject)
}
export { profile_scene }
