import { CSS3DObject } from 'three/CSS3DRenderer'
import { sceneDefinition } from '../logic/scene_logic.js'

/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const credits_scene = (position, rotation) => {
    // create a container for the form
    const container = document.createElement('div')
    container.className = 'credits-container'

    // add header
    const header = document.createElement('h1')
    header.innerHTML = 'Credits'
    header.style.color = 'goldenrod'
    header.style.textAlign = 'center'
    container.appendChild(header)

    // add credits
    const credits = document.createElement('p')
    credits.innerHTML = `
    <div style="text-align: center;">
        <p>@Requiaem</p>
        <p>@cguerreroto</p>
        <p>@smypl1</p>
        <p>@AgostinoMonti</p>
        <p>@frafantozzi</p>
        <p>@Bonni03</p>
    </div>`

    // add the form to the container
    container.appendChild(credits)

    // make the form a CSS3DObject
    const creditsObject = new CSS3DObject(container)
    creditsObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    creditsObject.rotation.set(rotation.x, rotation.y, rotation.z)
    sceneDefinition.cssScene.add(creditsObject)
}
export { credits_scene }
