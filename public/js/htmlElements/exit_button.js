const exit_button = () => {
    // add a new html button
    const button = document.createElement('button')
    button.innerHTML = 'Go Back'
    button.style.position = 'absolute'
    button.style.top = '10px'
    button.style.left = '10px'
    button.style.width = '100px'
    button.style.height = '50px'
    button.style.fontSize = '20px'
    button.style.fontWeight = 'bold'
    button.style.color = 'white'
    button.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
    button.style.border = '1px solid goldenrod'
    button.style.borderRadius = '10px'
    button.style.margin = '5px'
}

export { exit_button }



