import { CSS3DObject } from 'three/CSS3DRenderer'
import { onExitScene, sceneDefinition } from '../logic/scene_logic.js'
import { chat_api } from '../logic/chat_api.js'
import { unauthed_nav } from '../scenes/nav_scene.js'


/*************************/
/* Live Scene Definition */
/*************************/

// TODO: add login logic and side effects

// Socket auth event -> transition back to orbital view -> change nav signs to authed view

const logout_scene = (position, rotation) => {
    // create a container for the form
    const container = document.createElement('div')
    // make the container a fixed size
    container.style.width = '180px'
    container.style.height = '35px'


    // use ejs load the login page
    container.innerHTML = ejs.views_logout()

    // 
    container.querySelector("form.logout").addEventListener("submit", event => {
        event.preventDefault();
        chat_api.logout();
    })

    // make the form a CSS3DObject
    const formObject = new CSS3DObject(container)
    formObject.position.set(position.x * 500, position.y * 500, position.z * 500)
    //formObject.rotation.set(rotation.x, rotation.y, rotation.z)
    formObject.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w)
    formObject.scale.set(0.18, 0.18, 0.18)
    sceneDefinition.cssScene.add(formObject)

    // // add transform controls
    // const transformControls = new TransformControls(sceneDefinition.camera, sceneDefinition.renderer.domElement)
    // transformControls.attach(formObject)
    // sceneDefinition.cssScene.add(transformControls)

    // // add event listeners for position change
    // transformControls.addEventListener('dragging-changed', (event) => {
    //     // console log the position of the form
    //     console.log(formObject.position)
    //     // console log the rotation of the form
    //     console.log(formObject.quaternion)
    // })
}
export { logout_scene }
