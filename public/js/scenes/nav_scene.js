import * as THREE from 'three'
import { TextGeometry } from 'three/TextGeometry'
import { loadText } from '../utilities/font_loader.js'
import { loadAsset } from '../utilities/asset_loader.js'
import { login_scene } from '../htmlElements/login_scene.js'
import { logout_scene } from '../htmlElements/logout_scene.js'
import { leaderboard_scene } from '../htmlElements/leaderboard_scene.js'
import { chat_scene } from '../htmlElements/chat_scene.js'
import { credits_scene } from '../htmlElements/credits_scene.js'
import { profile_scene } from '../htmlElements/profile_scene.js'
import { pacman_scene } from '../htmlElements/pacman_scene.js'
import { sceneDefinition } from '../logic/scene_logic.js'
import { authModel } from '../logic/auth_model.js'

/*************************/
/* NavScene  Definition */
/*************************/

// A Dict of scenes that can be navigated to
// routs[scene.name] = {
//   position: THREE.Vector3,
//   rotation: THREE.quaternion,
//   callback: function,
//   isGame: bool
// }

const routs = {}

// Navigation Menu State
let counter = 2
let navPost = null;
let navSigns = [];
let navSign = null;
let navSignFont = null;

/***********************/
/*  Navigation  Menus  */
/***********************/

// The authorized navigation menu
const authed_nav = () => {
  counter = 2

  // remove the signs, if any
  navSigns.forEach((sign) => {
    sign.removeFromParent();
  })

  navSigns = []

  if (navPost != null) {
    playSign(navPost);
    leaderboardSign(navPost);
    chatSign(navPost);
    profileSign(navPost);
    creditsSign(navPost);
    logoutSign(navPost);
  } else {
    loadAsset(sceneDefinition.threeScene, 'assets/a_welcoming_sign.gltf', 'lampPost', (gltf) => {
      const lampPost = gltf.scene
      lampPost.position.set(-2, 1.45, 3.5) //-2, 1.45, 4
      lampPost.scale.set(0.7, 0.7, 0.7)
      navPost = lampPost
      lampPost.castShadow = true;
      lampPost.receiveShadow = true;

      const lampPostLight = new THREE.SpotLight(0xffff00, 0.75, 80, 1);
      lampPostLight.position.set(-1, 3, 0);
      lampPostLight.target = lampPost;
      // make the cone of the spotlight a little thinner
      lampPostLight.angle = Math.PI / 3;
      // increment fall off to make the light more focused
      lampPostLight.penumbra = 0.5;
      lampPostLight.castShadow = true;
      lampPost.add(lampPostLight);

      playSign(navPost);
      leaderboardSign(navPost);
      chatSign(navPost);
      profileSign(navPost);
      creditsSign(navPost);
      logoutSign(navPost);
    })
  }
}

// The unauthorized navigation menu
const unauthed_nav = () => {
  counter = 2

  // remove the signs, if any
  navSigns.forEach((sign) => {
    sign.removeFromParent();
  })

  navSigns = []

  // instantiate a new sign object
  // it takes you, when you click it, to the given position and rotation, and instantiates the given scene
  if (navPost != null) {
    loginSign(navPost);
    creditsSign(navPost);
  } else {
    loadAsset(sceneDefinition.threeScene, 'assets/a_welcoming_sign.gltf', 'lampPost', (gltf) => {
      const lampPost = gltf.scene
      lampPost.position.set(-3, 1.45, 3) //-2, 1.45, 4
      lampPost.scale.set(0.7, 0.7, 0.7)
      navPost = lampPost
      lampPost.castShadow = true;
      lampPost.receiveShadow = true;
      const lampPostLight = new THREE.SpotLight(0xffff00, 0.5, 100, 1);
      lampPostLight.position.set(-1, 3, 0);
      lampPostLight.target = lampPost;
      // make the cone of the spotlight a little thinner
      lampPostLight.angle = Math.PI / 4;
      // increment fall off to make the light more focused
      lampPostLight.penumbra = 0.75;
      lampPostLight.castShadow = true;
      lampPost.add(lampPostLight);

      loginSign(lampPost);
      creditsSign(lampPost);
    })
  }
}

/***********************/
/*  Navigation  Signs  */
/***********************/

// A sign that, when clicked
// will transform the camera
// and trigger a callback
const sign = (scene, name, position, rotation, callback, _isGame = false) => {
  if (navSign == null) {
    loadAsset(scene, 'assets/sign.gltf', name, (gltf) => {
      // create a rectangular sign to be placed on the cube (random size and color)
      const sign = gltf.scene
      navSign = sign.clone();
      console.log(gltf.scene)
      addSign(scene, name, position, rotation, callback, _isGame, sign)
    })
  } else {
    const sign = navSign.clone()
    sign.name = name
    scene.add(sign)
    addSign(scene, name, position, rotation, callback, _isGame, sign)
  }
}

const addSign = (scene, name, position, rotation, callback, _isGame = false, sign) => {
  navSigns.push(sign)

  routs[sign.name] = {
    position: position,
    rotation: rotation,
    callback: callback,
    isGame: _isGame
  }


  sign.position.set(0, -0.45 * counter++ + 2.2, 0.27)
  sign.rotation.y = Math.random() * (Math.PI / 12) * (Math.random() > 0.5 ? 1 : -1)
  sign.rotation.z = Math.random() * (Math.PI / 12) * (Math.random() > 0.5 ? 1 : -1)

  addText(name, sign);
}

const addText = (name, sign) => {
  if (navSignFont == null) {
    loadText((font) => {
      navSignFont = font
      // add text to the sign
      const text = new TextGeometry(name, {
        font: font,
        size: 0.1,
        height: 0.03,
        curveSegments: 10,
        bevelEnabled: true,
        bevelThickness: 0.01,
        bevelSize: 0.0055,
        bevelOffset: 0,
        bevelSegments: 3
      })

      // make pivot point the center of the text
      text.scale(0.75, 0.75, 0.75)
      text.computeBoundingBox()
      text.translate(
        -0.5 * (text.boundingBox.max.x - text.boundingBox.min.x),
        -0.75 * (text.boundingBox.max.y - text.boundingBox.min.y),
        0.15 * (text.boundingBox.max.z - text.boundingBox.min.z)
      )

      // generate a color with at least 75% brightness, and very saturated
      const color = new THREE.Color()
      color.setHSL(Math.random(), 0.5 + Math.random() * 0.5, 0.75 + Math.random() * 0.25)
      const textMaterial = new THREE.MeshStandardMaterial({ color: color })
      const textMesh = new THREE.Mesh(text, textMaterial)
      textMesh.position.set(0, 0.05, 0.01)

      sign.add(textMesh)

      const addLight = function () {
        const signLight = new THREE.SpotLight(color, 0.75, 0.85, 1, 0.55, 0.55);
        signLight.position.set(0, 0, 0.55);
        signLight.target = textMesh;
        signLight.castShadow = false;
        textMesh.add(signLight);
      }()
    })
  } else {
    // add text to the sign
    const text = new TextGeometry(name, {
      font: navSignFont,
      size: 0.1,
      height: 0.03,
      curveSegments: 10,
      bevelEnabled: true,
      bevelThickness: 0.01,
      bevelSize: 0.0055,
      bevelOffset: 0,
      bevelSegments: 3
    })

    // make pivot point the center of the text
    text.scale(0.75, 0.75, 0.75)
    text.computeBoundingBox()
    text.translate(
      -0.5 * (text.boundingBox.max.x - text.boundingBox.min.x),
      -0.75 * (text.boundingBox.max.y - text.boundingBox.min.y),
      0.15 * (text.boundingBox.max.z - text.boundingBox.min.z)
    )

    // generate a color with at least 75% brightness, and very saturated
    const color = new THREE.Color()
    color.setHSL(Math.random(), 0.5 + Math.random() * 0.5, 0.75 + Math.random() * 0.25)
    const textMaterial = new THREE.MeshStandardMaterial({ color: color })
    const textMesh = new THREE.Mesh(text, textMaterial)
    textMesh.position.set(0, 0.05, 0.01)

    sign.add(textMesh)

    const addLight = function () {
      const signLight = new THREE.SpotLight(color, 0.75, 0.85, 1, 0.55, 0.55);
      signLight.position.set(0, 0, 0.55);
      signLight.target = textMesh;
      signLight.castShadow = false;
      textMesh.add(signLight);
    }()
  }
}

// A sign that takes you to the credits
const creditsSign = (parent) => {
  sign(parent, 'Credits', new THREE.Vector3(0, 7, -5), new THREE.Quaternion(0, 0, 0, sceneDefinition.cameraInitialRot.w), (_) => {
    // the position of the sign up scene has to be opposite of the login scene
    const creditsPos = new THREE.Vector3(0, 7, -6)
    // rotate by 90 degrees around the y axis
    const creditsRot = new THREE.Quaternion(0, 0, 0, sceneDefinition.cameraInitialRot.w)

    credits_scene(creditsPos, creditsRot);
  })
}

// A sign that takes you to the login
const logoutSign = (parent) => {
  // what are the components of vector3
  // Vector3(x, y, z)
  sign(parent, 'Logout',
    new THREE.Vector3(2.88, 0.67, 3.35), // to the right of the lamp post
    new THREE.Quaternion(-0.14, -0.58, -0.10, 0.8),
    () => {
      const logoutPos = new THREE.Vector3(3.2, 0.68, 3.2)
      // rotate by 40 degrees around the y axis, and 10 degrees around the x axis
      const logoutRot = new THREE.Quaternion(-0.08, -0.57, -0.05, sceneDefinition.cameraInitialRot.w)

      logout_scene(logoutPos, logoutRot);
    })
}

// A sign that takes you to the login
const loginSign = (parent) => {
  // what are the components of vector3
  // Vector3(x, y, z)
  sign(parent, 'Login',
    new THREE.Vector3(2.88, 0.67, 3.35), // to the right of the lamp post
    new THREE.Quaternion(-0.14, -0.58, -0.10, 0.8),
    () => {
      const loginPos = new THREE.Vector3(3.2, 0.68, 3.2)
      // rotate by 40 degrees around the y axis, and 10 degrees around the x axis
      const loginRot = new THREE.Quaternion(-0.08, -0.57, -0.05, sceneDefinition.cameraInitialRot.w)

      login_scene(loginPos, loginRot);
    })
}

// A sign that takes you to the profile
const profileSign = (parent) => {
  sign(parent, 'Profile', new THREE.Vector3(2, 1.5, 0), new THREE.Quaternion(0, 0.707, 0, sceneDefinition.cameraInitialRot.w), () => {
    const profilePos = new THREE.Vector3(0.5, 1.70, -0.075)
    // rotate by 90 degrees around the y axis
    const profileRot = new THREE.Quaternion(0, Math.PI / 2, 0, sceneDefinition.cameraInitialRot.w)
    profile_scene(profilePos, profileRot, authModel.currentUser);
  })
}

// A sign that takes you to the chat
const chatSign = (parent) => {
  sign(parent, 'Chat Rooms', new THREE.Vector3(0, 7, -3), new THREE.Quaternion(0, 0, 0, sceneDefinition.cameraInitialRot.w), (_) => {
    // the position of the sign up scene has to be opposite of the login scene
    const chatPos = new THREE.Vector3(-0.40, 7.1, -5)
    // rotate by 90 degrees around the y axis
    const chatRot = new THREE.Quaternion(0, 0, 0, sceneDefinition.cameraInitialRot.w)

    chat_scene(chatPos, chatRot);
  })
}


// A sign that takes you to the leaderboard
const leaderboardSign = (parent) => {
  sign(parent, 'LeaderBoards', new THREE.Vector3(-1.5, 1, 0), new THREE.Quaternion(0, -0.707, 0, sceneDefinition.cameraInitialRot.w), () => {
    // the position of the sign up scene has to be opposite of the login scene
    const leaderPos = new THREE.Vector3(-0.5, 1.05, -0.075)
    // rotate by 90 degrees around the y axis
    const leaderRot = new THREE.Quaternion(0, - Math.PI / 2, 0, sceneDefinition.cameraInitialRot.w)

    leaderboard_scene(leaderPos, leaderRot);
  })
}

// A sign that takes you to the game
const playSign = (parent) => {
  sign(parent, 'Play Game', new THREE.Vector3(0, 1.2, 0.87), new THREE.Quaternion(0, 0, 0, sceneDefinition.cameraInitialRot.w), () => {
    // the position of the sign up scene has to be opposite of the login scene
    const gamePos = new THREE.Vector3(0, 1.21, 0.1)
    // rotate by 5 degrees around the x axis
    const gameRot = new THREE.Quaternion(-0.15, 0, 0, sceneDefinition.cameraInitialRot.w)

    pacman_scene(gamePos, gameRot);
  }, true)
}

export { unauthed_nav, authed_nav, routs }