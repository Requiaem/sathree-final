import * as THREE from 'three'
import { sceneDefinition } from '../logic/scene_logic.js';


/*************************/
/* Live Scene Definition */
/*************************/

const light_scene = () => {

  // ++++++++++ LOCAL LIGHTS ++++++++++

  // SpotLight (arcade machine):
  const arcadeMachineLight = new THREE.SpotLight(0xffff00, 0.75, 80, Math.PI / 2, 1, 1);
  arcadeMachineLight.position.set(0, 3, 3);
  arcadeMachineLight.target = sceneDefinition.threeScene;
  // make the cone of the spotlight a little thinner
  arcadeMachineLight.angle = Math.PI / 3;
  // increment fall off to make the light more focused
  arcadeMachineLight.penumbra = 0.5;
  sceneDefinition.threeScene.add(arcadeMachineLight);

  // ++++++++++ GENERAL LIGHTS ++++++++++


  // DirectionalLight (General): Directional light to the entire scene
  const directionalLight = new THREE.DirectionalLight(0xffffff, 0.005);
  // have this light shine from the top, angled at 45 degrees on both x and y axes
  directionalLight.position.set(1, 1, 1).normalize();
  // add directional light to the scene
  sceneDefinition.threeScene.add(directionalLight)

  // useful to visualize the light:
  // const dlHelper = new THREE.DirectionalLightHelper(light, 1);
  // sceneDefinition.threeScene.add(dlHelper);

  return directionalLight
}

export { light_scene }
