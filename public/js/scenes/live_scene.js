import * as THREE from 'three'
import { loadAsset } from '../utilities/asset_loader.js'
import { sceneDefinition } from '../logic/scene_logic.js';

/*************************/
/* Live Scene Definition */
/*************************/

const live_scene = () => {

    // Scene: Arcade machine Cabinet
    loadAsset(sceneDefinition.threeScene, 'assets/arcade_machine.gltf', 'arcademachine', (gltf) => {
    });

    // // Create the ground
	// 	var groundMat = new THREE.MeshBasicMaterial( {
    //         // new THREE.TextureLoader().load( 'assets/sand.jpeg' ),
    //         color: 0x091304
    //     }  );

	// 	//create the plane geometry
	// 	var geometry = new THREE.PlaneGeometry(200,200);

	// 	//create the ground form the geometry and material
	// 	var ground = new THREE.Mesh(geometry,groundMat);
    //     ground.rotation.x = -Math.PI/2;
	// 	sceneDefinition.threeScene.add(ground);
    
    // Scene: old table that contains a laptop for loging in.
    loadAsset(sceneDefinition.threeScene, 'assets/computersontable.gltf', 'alchemyTable', (gltf) => {
        const alchemyTable = gltf.scene;
        alchemyTable.scale.set(0.5, 0.5, 0.5);
        alchemyTable.position.set(3, 0, 3);
        alchemyTable.rotation.y = -Math.PI / 2.5;

        const tableLight = new THREE.SpotLight(0xffff00, 0.75, 80, Math.PI / 2, 1, 1);
        tableLight.position.set(1, 3, 1);
        tableLight.target = alchemyTable;
        // make the cone of the spotlight a little thinner
        tableLight.angle = Math.PI / 3;
        // increment fall off to make the light more focused
        tableLight.penumbra = 0.5;
        alchemyTable.add(tableLight);
    });

    loadAsset(sceneDefinition.threeScene, 'assets/plane.gltf', 'ground', (gltf) => {

    // // Scene: Chat booth inside telephone cabine.
    // loadAsset(sceneDefinition.threeScene, 'assets/chat_booth.gltf', 'chatBooth', (gltf) => {
    //     const chatBooth = gltf.scene;
    //     chatBooth.scale.set(0.7, 0.7, 0.7);
    //     chatBooth.rotation.y = Math.PI / 6;
    //     chatBooth.position.set(5.5, 0, 1.5);
    // });
    });

}

export { live_scene }
