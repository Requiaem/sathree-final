const button_scene = (text = "Button", callback = () => { }, counter, className = "genericButton") => {
  let top = 60 * counter + 10

  // add a new html button
  const button = document.createElement('button')
  button.innerHTML = text
  button.style.position = 'absolute'
  button.style.top = `${top}px`
  button.style.left = '10px'
  button.style.width = '100px'
  button.style.height = '50px'
  button.style.fontSize = '20px'
  button.style.fontWeight = 'bold'
  button.style.color = 'white'
  button.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
  button.style.border = '1px solid goldenrod'
  button.style.borderRadius = '10px'
  button.style.margin = '5px'
  button.style.zIndex = '100'
  button.className = className

  // add on appear effect
  button.addEventListener('mouseover', () => {
    button.style.transition = 'all 0.35s ease'
    button.style.transform = 'scale(1.2)'
    button.style.backgroundColor = 'goldenrod'
    button.style.color = 'black'
  })

  // add on disappear effect
  button.addEventListener('mouseout', () => {
    button.style.transition = 'all 0.35s ease'
    button.style.transform = 'scale(1)'
    button.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
    button.style.color = 'goldenrod'
  })

  // add the button to the dom
  document.body.appendChild(button)
  let buttons = document.querySelectorAll(".mapButton");

  // add a click event listener to the button
  button.addEventListener('click', (event) => {
    console.log("Button clicked")
    console.log("ciao")
    event.preventDefault();
    document.body.removeChild(button)
    buttons.forEach((button) => {
      button.remove();
    })
    callback();
  });
}

export { button_scene }



