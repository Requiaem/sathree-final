import { TransformControls } from 'three/TransformControls'
import { sceneDefinition } from '../logic/scene_logic.js';
import * as THREE from 'three';


/*************************/
/* Live Scene Definition */
/*************************/

const debug_scene = () => {
    // add a transform control to the scene for each asset
    const transformControls = new TransformControls(sceneDefinition.camera, sceneDefinition.threeRenderer.domElement);
    transformControls.addEventListener('dragging-changed', (event) => {
        sceneDefinition.cssControls.enabled = !event.value;
    });

    // add an axis helper to the scene
    const axisHelper = new THREE.AxesHelper(5);
    sceneDefinition.threeScene.add(axisHelper);

    // add a grid helper to the scene
    const gridHelper = new THREE.GridHelper(100, 100);
    sceneDefinition.threeScene.add(gridHelper);
} 

export { debug_scene }
