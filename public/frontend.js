let player = "";
let follower = [];
let followed = [];
let socket = null;

async function init() {
    let search = window.location.search;
    player = parse_parameter_value(search, "player");
    if (player == undefined) {
        player = "Anonymous Player";
    }
    document.getElementById("player").value = player;

    // await displayHighScores();

    await getPacmanGame();
  
    socket = io({
        extraHeaders: {
            "name": player
        }
    });

    socket.on('connect', () => { console.log("Connected to server"); });
    socket.on('disconnect', (reason) => { });
    socket.on('reconnect', (attemptNumber) => { });

    //receive a message from the server
    socket.on('message', (msg) => {
        console.log(msg);
    });

    socket.on('user.record', (record) => {
        displayRecord();
    });

    socket.on('user.edit', async (data) => {
        await getPacmanGame();
        // TODO complete
    });

    socket.on('follower.count', (data) => {
        follower = data;
        displayFollower();
    });

    socket.on('followed.count', (data) => {
        followed = data;
        displayFollowed();
    });
}

async function displayRecord() {
    // TODO: Display the record(s) of the player(s), 
    // if there is a page with high scores (aka Record) change add here as follows:. 

    // const record = await api.getHighScores();
    // const html = ejs.views_high_scores({ highScores });
    // document.getElementById("high-scores").innerHTML = html;
}

async function displayFollower() {
    // TODO: Display the follower(s) of the player(s),
}

async function displayFollowed() {
    // TODO: Display the followed(s) by the player(s),
}

async function getPacmanGame() {
    const game = await api.getPacmanGame();
    const html = ejs.views_pacman_game({ game });
    document.getElementById("pacman-game").innerHTML = html;
}


/**
 * Reads the value for a key from an URL encoded string.
 * @param {string} url A string consisting a URL with key/value pairs encoded like URL parameters.
 * @param {string} key The key to find in the string.
 * @returns The value for the key if it was found, undefined otherwise.
 */
 function parse_parameter_value(url, key) {
    let split = url.split('?');
    if (split.length >= 2) {
        let params = split[1].split('&');
        for (const param of params) {
            let equalsIdx = param.indexOf('=');
            if (equalsIdx >= 0) {
                let key_read = param.substring(0, equalsIdx);
                let value_read = param.substring(equalsIdx + 1);

                if (key == key_read) {
                    return value_read;
                }
            }
        }
    }
}