const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
let { model, auth } = require("../model");

// TODO: Implement the user routes handlers

router.get("/", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1].split("/")[0];
	console.log(token);
	// get user id from request body
	const _id = req.headers.authorization.split(" ")[1].split("/")[1];
	console.log(_id);


	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		// get user from database
		model.Users.findOne({ _id: new ObjectId(_id) })
			.then(user => {
				// check if the user exists
				if (user) {
					// return user
					res.status(200).json(user);
				} else {
					// return error
					res.status(404).json({ error: "User not found" });
				}
			}).catch(err => {
				// return error
				res.status(500).json({ error: err });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

// Get user by user name

router.get("/name/:name", (req, res) => {
	const token = req.headers.authorization.split(" ")[1].split("/")[0];
	console.log(token);
	// get user id from request body
	const _id = req.headers.authorization.split(" ")[1].split("/")[1];
	console.log(_id);

	const user_name = req.params.name;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		model.Users.findOne({ username: user_name })
			.then(user => {
				// check if the user exists
				if (user) {
					// return user
					res.status(200).json(user);
				} else {
					// return error
					res.status(404).json({ error: "User not found" });
				}
			}).catch(err => {
				// return error
				res.status(500).json({ error: err });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});




router.get("/friends", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1].split("/")[0];
	console.log(token);
	// get user id from request body
	const _id = req.headers.authorization.split(" ")[1].split("/")[1];
	console.log(_id);

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		model.Users.findOne({ _id: new ObjectId(_id) })
			.then(user => {
				if (user) {
					// map user.follower to ObjectId
					const followers = user.followers.map(id => new ObjectId(id));
					// map user.following to ObjectId
					const following = user.following.map(id => new ObjectId(id));
					// get friends from database, they are the user's whose ids are both in the following array and the followers array
					model.Users.find({
						$and: [
							{ _id: { $in: followers } },
							{ _id: { $in: following } }
						]
					}).toArray()
						.then(friends => {
							res.json(friends);
						}).catch(err => {
							res.status(500).json({ message: "Internal Server Error" });
						});
				} else {
					res.status(404).json({ message: "No Friends found" });
				}
			}).catch(err => {
				res.status(500).json({ message: "Internal Server Error" });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

router.get("/:id", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1];
	// get user id from request body
	const _id = req.params.id;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		model.Users.findOne({ _id: new ObjectId(_id) })
			.then(user => {
				if (user) {
					res.json(user);
				} else {
					res.status(404).json({ message: "User not found" });
				}
			}).catch(err => {
				res.status(500).json({ message: "Internal Server Error" });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

router.get("/search/:query", (req, res) => {
	const query = req.params.query;
	const words = query.split(" ");

	// find documents in the Users collection that match the query on either the username or the firstname or the lastname on either the full query or any of the words in the query
	model.Users.find({
		$or: [
			{ username: { $regex: query, $options: "i" } },
			{ firstname: { $regex: query, $options: "i" } },
			{ lastname: { $regex: query, $options: "i" } },
			{ username: { $in: words } },
			{ firstname: { $in: words } },
			{ lastname: { $in: words } }]
	}).toArray()
		.then(users => {
			res.json(users);
		}).catch(err => {
			res.status(500).json({ message: "Internal Server Error" });
		});
});



router.post("", (req, res) => {
	const { email, password, username, image_url, firstname, lastname, bio } = req.body;

	const emailExists = model.EmailExists(email);
	const usernameExists = model.UsernameExists(username);

	Promise.all([emailExists, usernameExists]).then(values => {
		if ((values[0] || values[1])) {
			res.status(409).json({ message: "Email or username already exists" });
			res.end();
		} else {
			// insert a new user into the Users collection and get the _id of the new user
			model.Users.insertOne({ email: email, password: password, username: username, image_url: image_url, firstname: firstname, lastname: lastname, bio: bio, followers: [], following: [] })
				.then(user => {
					// Authenticate the user
					const token = auth.Authorize(user.insertedId.toString());
					console.log(token);
					console.log(user);
					// Send the token back to the client
					res.json({ token, user });
				}).catch(err => {
					console.log(err);
					// There was an error inserting the user into the Users collection
					res.status(500).json({ error: err });
				});
		}
	})
});

router.put("/edit", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1];
	// get user id from request body
	const _id = req.body._id;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		// get user properties from request body
		const { email, password, username, image_url, firstname, lastname, bio } = req.body;
		// only update the properties that are not null
		const update = {};
		if (email) update.email = email;
		if (password) update.password = password;
		if (username) update.username = username;
		if (image_url) update.image_url = image_url;
		if (firstname) update.firstname = firstname;
		if (lastname) update.lastname = lastname;
		if (bio) update.bio = bio;

		// update the user in the Users collection
		model.Users.updateOne({ _id: new ObjectId(_id) },
			{ $set: update })
			.then(user => {
				// return the updated user
				res.status(200).json(user);
			}).catch(err => {
				// return error
				res.status(500).json({ error: err });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

router.put("/follow/:id", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1];
	// get user id from request body
	const _id = req.body._id;
	// get user id to follow from request params
	const id = req.params.id;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		// update the user in the Users collection
		model.Users.updateOne({ _id: new ObjectId(_id) },
			{ $addToSet: { following: id } })
			.then(user => {
				model.Users.updateOne({ _id: new ObjectId(id) },
					{ $addToSet: { followers: _id } })
					.then(user => {
						// return the updated user
						res.status(200).json(true);
					}).catch(err => {
						// return error
						res.status(500).json(false);
					});
			}).catch(err => {
				// return error
				res.status(500).json(false);
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

router.put("/unfollow/:id", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1];
	// get user id from request body
	const _id = req.body._id;

	// get user id to unfollow from request params
	const id = req.params.id;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		// update the user in the Users collection
		model.Users.updateOne({ _id: new ObjectId(_id) },
			{ $pull: { following: id } })
			.then(user => {
				model.Users.updateOne({
					_id: new ObjectId(id)
				},
					{ $pull: { followers: _id } })
					.then(user => {
						// return the updated user
						res.status(200).json(true);
					}).catch(err => {
						// return error
						res.status(500).json(false);
					});
			}).catch(err => {
				// return error
				res.status(500).json(false);
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

router.delete("/", (req, res) => {
	// get uuid token from request header
	const token = req.headers.authorization.split(" ")[1];
	// get user id from request body
	const _id = req.body._id;

	// check if the user is authorized
	if (auth.IsAuthorized(token, _id)) {
		let ops = [];
		// delete the user from the Users collection
		ops.push(model.Users.deleteOne({ _id: new ObjectId(_id) }));
		// delete the user's records from the Records collection
		ops.push(model.Records.deleteMany({ _user_id: _id }));
		// delete the user's id from the followers array of the users they follow
		ops.push(model.Users.updateMany({ following: _id }, { $pull: { following: _id } }));
		// delete the user's id from the following array of the users that follow them
		ops.push(model.Users.updateMany({ followers: _id }, { $pull: { followers: _id } }));
		// delete the user's id from the _party_ids array of the chats they are in
		ops.push(model.ChatRooms.updateMany({ _party_ids: _id }, { $pull: { _party_ids: _id } }));

		Promise.all(ops)
			.then(() => {
				// delete chat rooms that have less than 2 members
				model.ChatRooms.deleteMany({ members: { $lt: 2 } })
					.then(() => {
						// return success
						res.status(200).json(true);
					}).catch(err => {
						// return error
						res.status(500).json(false);
					});
			}).catch(err => {
				console.log(err);
				// return error
				res.status(500).json({ error: err });
			});
	} else {
		// return error
		res.status(401).json({ error: "Unauthorized" });
	}
});

module.exports = router;