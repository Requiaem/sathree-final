/*~~~~~~~~~~~~~~~~~~~~~~~~~~
	Required Dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

const fs = require('fs');

/*~~~~~~~~~~~~~~~~~~~~~
	Local Constants
~~~~~~~~~~~~~~~~~~~~~~*/

const dirEntries = fs.readdirSync(__dirname);
const base = __dirname + '/';
const routers = {};

/*~~~~~~~~~~~~~~~~~~~~~
	Routers Loading
~~~~~~~~~~~~~~~~~~~~~~*/

try {
    dirEntries.forEach(function (dirEntry) {
        const stats = fs.statSync(base + dirEntry);
        //try to load router of dir
        try {
            if (stats.isDirectory()) {

                const router = require(base + dirEntry + '/router');
                //add router to our list of routers;
                routers[dirEntry] = router;

            } else {
                const router = require(base + dirEntry);
                //add router to our list of routers;
                routers[dirEntry.replace(/.js$/, "")] = router;
            }
        } catch (err) {
            console.log('\x1b[33m%s\x1b[0m', 'Could not get router for ' + dirEntry);
            console.log('\x1b[31m%s\x1b[0m', err.toString() + err.stack);
        }
    });
} catch (err) {
    console.log('\x1b[33m%s\x1b[0m', 'Error while loading routers');
    console.log('\x1b[31m%s\x1b[0m', err.toString() + err.stack);
} finally {
    module.exports = routers;
}