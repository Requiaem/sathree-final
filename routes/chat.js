const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
let { model, auth } = require("../model");

// TODO: Implement the chat routes handlers

router.get("", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1].split("/")[0];
  console.log(token);
  // get user id from request body
  const _id = req.headers.authorization.split(" ")[1].split("/")[1];
  console.log(_id);

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // get if _id is in the Chat._party_ids array
    model.ChatRooms.find({ _party_ids: _id }).toArray()
      .then(chatrooms => {
        // check if the user exists
        if (chatrooms) {
          // return user
          res.status(200).json(chatrooms);
        } else {
          // return error
          res.status(404).json({ error: "User not found" });
        }
      }
      ).catch(err => {
        // return error
        res.status(500).json({ error: err });
      });
  } else {
    // return error
    res.status(401).json({ error: "Unauthorized" });
  }
});

router.get("/:id", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1].split("/")[0];
  console.log(token);
  // get user id from request body
  const _id = req.headers.authorization.split(" ")[1].split("/")[1];
  console.log(_id);

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // get if _id is in the Chat._party_ids array
    model.ChatRooms.findOne({ _id: new ObjectId(req.params.id), _party_ids: _id })
      .then(chatroom => {
        // return chatroom
        res.status(200).json(chatroom);
      }
      ).catch(err => {
        // return error
        res.status(500).json({ error: err });
      });
  } else {
    // return error
    res.status(401).json({ error: "Unauthorized" });
  }
});

router.put("/:id/message", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1];
  // get user id from request body
  const _id = req.body._id;

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // get if _id is in the Chat._party_ids array
    model.ChatRooms.findOne({ _id: new ObjectId(req.params.id), _party_ids: _id })
      .then(async chatroom => {
        // get the message from the request body

        let user = await model.Users.findOne({ _id: new ObjectId(_id) }); // add user name in history
        const message = {
          text: req.body.text,
          user_fullname: user.firstname + " " + user.lastname,
          _user_id: _id,
          timestamp: new Date()
        }

        // add the message to the chatroom
        chatroom.history.push(message);

        // update the chatroom in the database
        model.ChatRooms.updateOne({ _id: new ObjectId(req.params.id) }, { $set: chatroom })
          .then(result => {
            // return chatroom
            res.status(200).json(chatroom);
          }).catch(err => {
            // return error
            res.status(500).json({ error: err });
          });
      }
      ).catch(err => {
        // return error
        res.status(500).json({ error: err });
      });
  }
});

router.post("/new/:users", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1];
  // get user id from request body
  const _id = req.body._id;

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // for each user in the users array, check if they exist in the database
    const found = [];
    const users = req.params.users.split(",");
    users.forEach(user => {
      found.push(model.Users.findOne({ _id: new ObjectId(user) }));
    });

    const added = [_id];
    // only add users if they exist in the database
    Promise.all(found).then(users => {
      users.forEach(user => {
        if (user) {
          added.push(user._id.toString());
        }
        // create the chatroom
        const chatroom = {
          name: req.body.name,
          _party_ids: added,
          members: added.length,
          history: []
        }

        // insert the chatroom into the database
        model.ChatRooms.insertOne(chatroom)
          .then(result => {
            // return chatroom
            res.status(200).json(result);
          }).catch(err => {
            // return error
            res.status(500).json({ error: err });
          });
      });
    }).catch(err => {
      // return error
      res.status(500).json({ error: err });
    });

  } else {
    // return error
    res.status(401).json({ error: "Unauthorized" });
  }
});

router.put("/add/:id/:users", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1];
  // get user id from request body
  const _id = req.body._id;

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // get if _id is in the Chat._party_ids array
    model.ChatRooms.findOne({ _id: new ObjectId(req.params.id), _party_ids: _id })
      .then(chatroom => {
        // if _id is not chatroom._party_ids[0], the user is not the owner of the chatroom
        if (chatroom._party_ids[0] !== _id) {
          // return error
          res.status(401).json({ error: "Unauthorized" });
        } else {
          // for each user in the users array, check if they exist in the database
          const found = [];
          // get users from the request params
          const users = req.params.users.split(",");
          users.forEach(user => {
            found.push(model.Users.findOne({ _id: new ObjectId(user) }));
          });

          const added = [];
          // only add users if they exist in the database
          Promise.all(found).then(users => {
            users.forEach(user => {
              if (user) {
                added.push(user._id.toString());
              }
            });
            if (added.length > 0) {
              // filter out the users that are already in the chatroom
              const filtered = added.filter(user => !chatroom._party_ids.includes(user));
              // add the users from the chatroom
              chatroom._party_ids.push(...filtered);
              // update chatroom members
              chatroom.members = chatroom._party_ids.length;

              // update the chatroom _party_ids
              model.ChatRooms.updateOne({ _id: new ObjectId(req.params.id) }, { $set: chatroom })
                .then(chatroom => {
                  // return chatroom
                  res.status(200).json(chatroom);
                }
                ).catch(err => {
                  // return error
                  res.status(500).json({ error: err });
                });

            } else {
              // return bad request
              res.status(400).json({ error: "Bad Request" });
            }
          }).catch(err => {
            // return error
            res.status(500).json({ error: err });
          });
        }
      }).catch(err => {
        // return error
        res.status(500).json({ error: err });
      });
  } else {
    // return error
    res.status(401).json({ error: "Unauthorized" });
  }
});

router.put("/remove/:id/:users", (req, res) => {
  // get uuid token from request header
  const token = req.headers.authorization.split(" ")[1];
  // get user id from request body
  const _id = req.body._id;

  // check if the user is authorized
  if (auth.IsAuthorized(token, _id)) {
    // get if _id is in the Chat._party_ids array
    model.ChatRooms.findOne({ _id: new ObjectId(req.params.id), _party_ids: _id })
      .then(chatroom => {
        // if _id is not chatroom._party_ids[0], the user is not the owner of the chatroom
        if (chatroom._party_ids[0] !== _id) {
          // return error
          res.status(401).json({ error: "Unauthorized" });
        } else {
          // get users from the request params
          const users = req.params.users.split(",");
          if (users) {
            // remove the users from the chatroom
            chatroom._party_ids = chatroom._party_ids.filter(user => !users.includes(user));
            // update chatroom members
            chatroom.members = chatroom._party_ids.length;

            // if the chatroom has less than 2 members, delete the chatroom
            if (chatroom.members < 2) {
              model.ChatRooms.deleteOne({ _id: new ObjectId(req.params.id) })
                .then(result => {
                  // return chatroom
                  res.status(200).json(result);
                }).catch(err => {
                  // return error 
                  res.status(500).json({ error: err });
                });
            } else {

              // update the chatroom _party_ids
              model.ChatRooms.updateOne({ _id: new ObjectId(req.params.id) }, { $set: chatroom })
                .then(chatroom => {
                  // return chatroom
                  res.status(200).json(chatroom);
                }
                ).catch(err => {
                  // return error
                  res.status(500).json({ error: err });
                });
            }
          } else {
            // return bad request
            res.status(400).json({ error: "Bad Request" });
          }
        }
      }).catch(err => {
        // return error
        res.status(500).json({ error: err });
      });
  } else {
    // return error
    res.status(401).json({ error: "Unauthorized" });
  }
});

module.exports = router;