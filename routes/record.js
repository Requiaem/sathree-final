const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
let { model, auth } = require("../model");

// TODO: Implement the record routes handlers

router.get("/", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    // get user id from request body
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {
        // get user from database
        model.Users.findOne
        model.Records.find({ _user_id: _id }).toArray()
            .then(user => {
                // check if the user exists
                if (user) {
                    // return user
                    res.status(200).json(user);
                } else {
                    // return error
                    res.status(404).json({ error: "User not found" });
                }
            }).catch(err => {
                // return error
                res.status(500).json({ error: err });
            });
    } else {
        // return error
        res.status(401).json({ error: "Unauthorized" });
    }
});

router.get("/user/:id/:game", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {
        const _user_id = req.params.id;
        const game = req.params.game;
        model.Records.find({ _user_id: _user_id, _game_id: game }).toArray()
            .then(records => {
                if (records) {
                    res.json(records);
                } else {
                    res.status(404).json({ message: "Records not found" });
                }
            }).catch(err => {
                res.status(500).json({ message: "Internal Server Error" });
            });
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
});

router.get("/:game/:level", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    // get user id from request body
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {
        const game = req.params.game;
        const level = req.params.level;

        model.Records.find({ _game_id: game, level: level }).toArray()
            .then(records => {
                if (records) {
                    res.json(records);
                } else {
                    res.status(404).json({ message: "Records not found" });
                }
            }
            ).catch(err => {
                res.status(500).json({ message: "Internal Server Error" });
            });
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
});

router.get("/:game", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    // get user id from request body
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {
        const game = req.params.game;

        model.Records.find({ _game_id: parseInt(game) }).toArray()
            .then(records => {
                if (records) {
                    res.json(records);
                } else {
                    res.status(404).json({ message: "Records not found" });
                }
            }
            ).catch(err => {
                res.status(500).json({ message: "Internal Server Error" });
            });
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
});

router.get("/friends/:game", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    // get user id from request body
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {

        const game = req.params.game;
        const level = req.params.level;

        // get friends by intersecting followers and following arrays
        model.Users.findOne({ _id: new ObjectId(_id) })
            .then(user => {
                const friends = user.followers.filter(follower => user.following.includes(follower));

                // get records of friends
                model.Records.find({ _user_id: { $in: friends }, _game_id: game, level: level }).toArray()
                    .then(records => {
                        if (records) {
                            res.json(records);
                        } else {
                            res.status(404).json({ message: "Records not found" });
                        }
                    }).catch(err => {
                        res.status(500).json({ message: "Internal Server Error" });
                    });
            }
            ).catch(err => {
                res.status(500).json({ message: "Internal Server Error" });
            }
            );
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
});

router.post("", (req, res) => {
    // get uuid token from request header
    const token = req.headers.authorization.split(" ")[1].split("/")[0];
    // get user id from request body
    const _id = req.headers.authorization.split(" ")[1].split("/")[1];

    // check if the user is authorized
    if (auth.IsAuthorized(token, _id)) {
        // create new record
        const record = {
            username: req.body.username,
            _user_id: _id,
            _game_id: req.body._game_id,
            points: req.body.points,
            level: req.body.level,
            time: req.body.time
        };

        // save record to database in the Records collection
        model.Records.insert(record).then(record => {
            // return record
            res.status(200).json(record);
        }).catch(err => {
            // return error
            res.status(500).json({ error: err });
        });
    } else {
        // return error
        res.status(401).json({ error: "Unauthorized" });
    }
});

module.exports = router;